package gov.lbl.spot.spade.policy;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.config.Policy;
import gov.lbl.nest.spade.policy.FailedPolicyException;
import gov.lbl.nest.spade.policy.WrappingPolicy;
import gov.lbl.nest.spade.registry.ParseException;
import gov.lbl.nest.spade.services.MetadataManager;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;
import gov.lbl.spot.spade.metadata.bl1232.BL1232Metadata;
import gov.lbl.spot.spade.metadata.bl733.BL733Metadata;
import gov.lbl.spot.spade.metadata.bl832.BL832Metadata;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import ncsa.hdf.object.Attribute;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.Metadata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the {@link WrappingPolicy} instance in order to Wrap
 * and Unwrap ALS HDF5 files.
 * 
 * @author patton
 */
public class SpotWrapping implements
                         WrappingPolicy {

    // public static final member data

    /**
     * The suffix for compressed files.
     */
    public static final String WRAPPED_SUFFIX = ".h5";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SpotWrapping.class);

    /**
     * The format of the date for the stage_date.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    /**
     * The {@link DateFormat} instance to creating the stage_date.
     */
    private static DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    static {
        DATE_FORMATTER.setCalendar(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
    }

    /**
     * True if the unwrapping is done by an external script.
     */
    private static final boolean USE_UNWRAP_SCRIPT = true;

    // private static member data

    // private instance member data

    /**
     * The {@link MessageFormat} pattern that will be used to construct the
     * extract command line.
     */
    private final String extractCmd;

    /**
     * The {@link MessageFormat} pattern that will be used to construct the
     * unwrap command line.
     */
    private final String unwrapCmd;

    /**
     * The {@link MessageFormat} pattern that will be used to construct the
     * unwrap command line.
     */
    private final String wrapCmd;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param policy
     *            the {@link Policy} instance used to describe the instance to
     *            be created.
     */
    public SpotWrapping(final Policy policy) {
        extractCmd = policy.getParameter("extract");
        if (null == extractCmd) {
            LOG.warn("\"extract\" was not specified in action declaration");
        }

        unwrapCmd = policy.getParameter("unwrap");
        if (null == unwrapCmd) {
            LOG.warn("\"unwrap\" was not specified in action declaration");
        }

        wrapCmd = policy.getParameter("wrap");

        if (null == wrapCmd) {
            LOG.warn("\"bl832-wrap\" was not specified in action declaration");
        }
    }

    // instance member method (alphabetic)

    @Override
    public boolean extractMetadata(final File wrappedFile,
                                   final File directory,
                                   final String bundle,
                                   final String suffix) throws IOException,
                                                       InterruptedException {
        final String[] files = new String[] { wrappedFile.getAbsolutePath(),
                                             directory.getAbsolutePath(),
                                             bundle,
                                             suffix };
        final MessageFormat request = new MessageFormat(extractCmd);
        final String cmdString = request.format(files);
        final String[] cmdArray = new String[] { "bash",
                                                "-c",
                                                cmdString };
        final Command command = new Command(cmdArray);
        command.execute();
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            throw new IOException(cmdException);
        }
        return true;
    }

    @Override
    public String getSuffix() {
        return WRAPPED_SUFFIX;
    }

    @Override
    public boolean getMetadataRequired() {
        return true;
    }

    @Override
    public void unwrap(final File wrappedFile,
                       final File directory,
                       final String bundle,
                       final String suffix) throws IOException,
                                           InterruptedException,
                                           FailedPolicyException {
        final SpotMetadata spotMetadata = null;
        try {
            if (USE_UNWRAP_SCRIPT) {
                final String[] files = new String[] { wrappedFile.getAbsolutePath(),
                                                     directory.getAbsolutePath(),
                                                     bundle,
                                                     suffix };
                final MessageFormat request = new MessageFormat(unwrapCmd);
                final String cmdString = request.format(files);
                final String[] cmdArray = new String[] { "bash",
                                                        "-c",
                                                        cmdString };
                final Command command = new Command(cmdArray);
                command.execute();
                final ExecutionFailedException cmdException = command.getCmdException();
                if (null != cmdException) {
                    throw cmdException;
                }
            } else {
                final FileFormat format = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
                if (null == format) {
                    throw new RuntimeException("HDF5 not supported");
                }
                // System.out.println(wrappedFile.getPath());
                final FileFormat h5file = format.createInstance(wrappedFile.getPath(),
                                                                FileFormat.READ);
                try {
                    h5file.open();
                    final HObject root = h5file.get("/");
                    @SuppressWarnings("unchecked")
                    final List<Metadata> metadata = root.getMetadata();
                    for (final Metadata data : metadata) {
                        if (data instanceof Attribute) {
                            final Attribute attribute = (Attribute) data;
                            final String name = attribute.getName();
                            if ("owner".equals(name)) {
                                if (null == spotMetadata) {
                                    // spotMetadata = new SpotMetadata();
                                }
                            }
                        }
                    }
                    if (null != spotMetadata) {
                        // TODO: Complete
                    }
                } finally {
                    h5file.close();
                }
            }
        } catch (Exception e) {
            throw new FailedPolicyException(e);
        }
    }

    @Override
    public void wrap(final File dataFile,
                     final File metadataFile,
                     final File wrappedFile) throws IOException,
                                            InterruptedException,
                                            FailedPolicyException {
        try {
            final SpotMetadata metadata = SpotMetadataManager.readMetaData(metadataFile);
            final String[] files;

            if (metadata instanceof BL733Metadata) {
                final List<String> keywords = ((BL733Metadata) metadata).getKeywords();
                final StringBuffer sb = new StringBuffer();

                if (null != keywords) {
                    for (final String keyword : keywords) {
                        sb.append("-k ");
                        sb.append(keyword);
                        sb.append(" ");
                    }
                }

                files = new String[] { wrappedFile.getAbsolutePath(),
                                      sb.toString() + dataFile.getAbsolutePath(),
                                      metadata.getUuid()
                                          .toString(),
                                      metadata.getFacility(),
                                      metadata.getEndStation(),
                                      metadata.getOwner(),
                                      metadata.getDataset(),
                                      metadata.getStage(),
                                      DATE_FORMATTER.format(metadata.getStageDateAndTime()) };
            } else if (metadata instanceof BL832Metadata || metadata instanceof BL1232Metadata) {
                files = new String[] { wrappedFile.getAbsolutePath(),
                                      dataFile.getAbsolutePath(),
                                      metadata.getUuid()
                                          .toString(),
                                      metadata.getFacility(),
                                      metadata.getEndStation(),
                                      metadata.getOwner(),
                                      metadata.getDataset(),
                                      metadata.getStage(),
                                      DATE_FORMATTER.format(metadata.getStageDateAndTime()) };
            } else {
                throw new RuntimeException("unsupported metadata " + metadata.getClass());
            }

            final MessageFormat request = new MessageFormat(wrapCmd);
            final String cmdString = request.format(files);
            /*
             * final String[] cmdArray = new String[] { "bash", "-c", cmdString
             * + " > " + metadata.getEndStation() + ".txt"};
             */

            final String[] cmdArray = new String[] { "bash",
                                                    "-c",
                                                    cmdString };

            final Command command = new Command(cmdArray);
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                throw cmdException;
            }
        } catch (ParseException e) {
            throw new FailedPolicyException(e);
        } catch (ExecutionFailedException e) {
            throw new FailedPolicyException(e);
        }
    }

    @Override
    public void setMetadataManager(final MetadataManager manager) {
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
