package gov.lbl.spot.lcls;

import gov.lbl.nest.spade.metadata.Metadata1Impl;
import gov.lbl.nest.spade.policy.FailedPolicyException;
import gov.lbl.nest.spade.policy.PlacingPolicy;
import gov.lbl.nest.spade.registry.ParseException;
import gov.lbl.nest.spade.services.MetadataManager;
import gov.lbl.spot.spade.metadata.lcls.LclsMetadata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This class implements the {@link PlacingPolicy} instance so that files are
 * placed with respect to their facility/end_stattion/owner/dataset/stage
 * 
 * @author patton
 */
public class LclsPlacing implements
                        PlacingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link MetadataManager} instance used by this object.
     */
    private MetadataManager metadataManager;

    // constructors

    // instance member method (alphabetic)

    @Override
    public File getArchivePlacement(File metadataFile) throws FailedPolicyException {
        return getMetadataPlacement(metadataFile);
    }

    @Override
    public File getCompressedPlacement(String fileName,
                                       File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getDataPlacement(String fileName,
                                 File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getMetadataPlacement(File metadataFile) {
        return new File(path(metadataFile),
                        metadataFile.getName());
    }

    @Override
    public File getWrappedPlacement(String fileName,
                                    File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    /**
     * Returns the directory indicated by the {@link Metadata1Impl} instance.
     * 
     * @param metadataFile
     *            the cached metadata file
     * @return the directory indicated by the {@link Metadata1Impl} instance.
     */
    private File path(final File metadataFile) {
        LclsMetadata metadata;
        try {
            metadata = (LclsMetadata) metadataManager.createMetadata(metadataFile);
            final String endStation = metadata.getEndStation();
            if (null == endStation) {
                throw new IllegalArgumentException("No end_station specified in metadata");
            }
            final String experimentName = metadata.getExperimentName();
            if (null == experimentName) {
                throw new IllegalArgumentException("No experiment_name specified in metadata");
            }
            return new File(new File(endStation,
                                     experimentName),
                            "xtc");
        } catch (ParseException e) {
            throw new IllegalArgumentException("Metadata can not be parsed",
                                               e);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setMetadataManager(MetadataManager manager) {
        metadataManager = manager;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
