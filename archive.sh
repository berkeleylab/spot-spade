#!/bin/bash
# A simple script to create a tarball for distribution

if [ "X" = "X${1}" ] ; then
    echo usage: ${0} version [tag]
    exit 1
fi
SPOT_SPADE_VERSION=${1}
if [ "X" = "X${2}" ] ; then
    SPOT_SPADE_TAG=${SPOT_SPADE_VERSION}
else
    SPOT_SPADE_TAG=${2}
fi

git archive --format=tar --prefix=spot-spade-${SPOT_SPADE_VERSION}/ ${SPOT_SPADE_TAG} | gzip > spot-spade-${SPOT_SPADE_VERSION}.tgz
