package gov.lbl.spot.spade.metadata.aps;

import javax.xml.bind.annotation.XmlRootElement;

import gov.lbl.nest.spade.registry.Metadata;
import gov.lbl.spot.spade.metadata.SpotMetadata;

/**
 * The is the {@link Metadata} implementation for LCLS experiments.
 * 
 * @author patton
 */
@XmlRootElement(name = "aps-metadata")
public class ApsMetadata extends
                         SpotMetadata {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
