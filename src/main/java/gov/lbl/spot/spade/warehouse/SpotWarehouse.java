package gov.lbl.spot.spade.warehouse;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Priority;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.WarehouseDefinition;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import gov.lbl.nest.spade.warehouse.Location;
import gov.lbl.nest.spade.warehouse.Warehouse;

/**
 * This class implements the {@link Warehouse} interface use the ALS MongoDB.
 * 
 * @author patton
 */
@Stateless
@Alternative
@Priority(value = 0)
public class SpotWarehouse implements
                           Warehouse {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The file suffix indicating HDF5 files.
     */
    private static final String HDF5_SUFFIX = ".h5";

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private MongoDatabase mongoDb;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected SpotWarehouse() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param configuration
     *            the {@link Configuration} instance used by the new instance.
     */
    SpotWarehouse(MongoDatabase mongoDb) {
        this.mongoDb = mongoDb;
    }

    // instance member method (alphabetic)

    @Override
    public Object add(String bundle,
                      File cachedData,
                      File cachedMeta,
                      File cachedWrapped,
                      File cachedCompressed,
                      File warehousedData,
                      File warehousedMeta,
                      File warehousedWrapped,
                      File warehousedCompressed) throws InterruptedException,
                                                 IOException {
        final File resolvedWarehoused;
        final String warehouseId;
        if (warehousedWrapped.isAbsolute()) {
            resolvedWarehoused = warehousedWrapped;
            warehouseId = warehousedWrapped.getPath();
        } else {
            resolvedWarehoused = new File(getRoot(),
                                          warehousedWrapped.getPath());
            warehouseId = "/" + warehousedWrapped.getPath();
        }

        final File directory = resolvedWarehoused.getParentFile();
        if (!directory.exists()) {
            directory.mkdirs();
        }
        if (directory.exists()) {
            LocalhostTransfer.copy(cachedWrapped,
                                   resolvedWarehoused);
            // final Properties props = getMongoProperties();
            final String[] cmdArray = new String[] { "/global/project/projectdirs/als/prod/als_framework/bin/load.sh",
                                                     resolvedWarehoused
                                                         .getPath() /*
                                                                     * , props .
                                                                     * getProperty
                                                                     * (
                                                                     * MONGO_HOST
                                                                     * ) , props
                                                                     * .
                                                                     * getProperty
                                                                     * (
                                                                     * MONGO_PORT
                                                                     * ) , props
                                                                     * .
                                                                     * getProperty
                                                                     * (
                                                                     * WAREHOSE_COLLECTION
                                                                     * ) , props
                                                                     * .
                                                                     * getProperty
                                                                     * (
                                                                     * MONGO_USER
                                                                     * ) , props
                                                                     * .
                                                                     * getProperty
                                                                     * (
                                                                     * MONGO_PASSWD
                                                                     * )
                                                                     */ };
            final Command command = new Command(cmdArray);
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                throw new IOException(cmdException);
            }
            return warehouseId;
        }
        return null;
    }

    @Override
    public Location getCompressed(String warehouseId) {
        return null;
    }

    @Override
    public List<Location> getData(List<String> warehouseIds) {
        return null;
    }

    @Override
    public Location getData(String warehouseId) {
        return null;
    }

    @Override
    public Location getLocation(String warehouseId) {
        final String wrappedFile = getWrapped(warehouseId).getData();
        return new Location(warehouseId,
                            wrappedFile,
                            null,
                            wrappedFile,
                            null,
                            null);
    }

    @Override
    public Location getMetadata(String warehouseId) {
        return null;
    }

    @Override
    public List<Location> getModifiedSince(Date since,
                                           Date before,
                                           int maxCount) {
        return null;
    }

    @Override
    public List<Location> getModifiedSince(Date dateTime,
                                           int maxCount) {
        return null;
    }

    /**
     * Returns the root of the warehouse if one is defined, <code>null</code>
     * otherwise.
     * 
     * @return the root of the warehouse if one is defined, <code>null</code>
     *         otherwise.
     */
    private File getRoot() {
        final WarehouseDefinition definition = configuration.getWarehouseDefinition();
        if (null == definition) {
            return null;
        }
        final String root = definition.getRoot();
        if (null == root) {
            return null;
        }
        return new File(root);
    }

    @Override
    public Location getWrapped(String warehouseId) {
        final String filenameToUse;
        if (warehouseId.endsWith(HDF5_SUFFIX)) {
            filenameToUse = warehouseId;
        } else {
            filenameToUse = warehouseId + HDF5_SUFFIX;
        }
        final MongoCollection<Document> fsmetadata = mongoDb.getCollection("fsmetadata");
        final Bson query = Filters.eq("_id",
                                      filenameToUse);
        MongoCursor<Document> cursor = (fsmetadata.find(query)).iterator();
        if (!cursor.hasNext()) {
            return null;
        }
        Document idObject = cursor.next();
        Document fsObject = idObject.get("fs",
                                         Document.class);
        if (null == fsObject) {
            return null;
        }
        String value = fsObject.getString("phyloc");
        if (null == value) {
            return null;
        }
        return new Location(warehouseId,
                            value);
    }

    @Override
    public Object move(String bundle,
                       File movedData,
                       File movedMeta,
                       File movedWrapped,
                       File movedCompressed) throws InterruptedException,
                                             IOException {
        throw new UnsupportedOperationException();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

}
