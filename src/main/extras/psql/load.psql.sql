--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dispatch; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE dispatch (
    dispatchkey integer NOT NULL,
    whenabandoned timestamp without time zone,
    whencompleted timestamp without time zone,
    whenstarted timestamp without time zone,
    destination_neighborkey integer NOT NULL,
    ticketedfile_ticketedfilekey integer NOT NULL
);


ALTER TABLE public.dispatch OWNER TO spade;

--
-- Name: entry; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE entry (
    entrykey integer NOT NULL,
    identity character varying(255),
    whenlastmodified timestamp without time zone,
    compressedpath_pathkey integer,
    datapath_pathkey integer,
    metapath_pathkey integer,
    wrappedpath_pathkey integer
);


ALTER TABLE public.entry OWNER TO spade;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: spade
--

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO spade;

--
-- Name: hierarchicalpath; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE hierarchicalpath (
    pathkey integer NOT NULL,
    cachedpath character varying(255),
    name character varying(255),
    parent_pathkey integer NOT NULL
);


ALTER TABLE public.hierarchicalpath OWNER TO spade;

--
-- Name: inboundregistration; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE inboundregistration (
    remoteid character varying(255),
    registrationkey integer NOT NULL,
    deliverer_neighborkey integer NOT NULL
);


ALTER TABLE public.inboundregistration OWNER TO spade;

--
-- Name: lastwatched; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE lastwatched (
    lastwatchedkey integer NOT NULL,
    lastdatetime timestamp without time zone NOT NULL,
    role character varying(255) NOT NULL
);


ALTER TABLE public.lastwatched OWNER TO spade;

--
-- Name: localregistration; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE localregistration (
    registrationkey integer NOT NULL,
    localid character varying(255)
);


ALTER TABLE public.localregistration OWNER TO spade;

--
-- Name: neighbor; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE neighbor (
    neighborkey integer NOT NULL,
    email character varying(255)
);


ALTER TABLE public.neighbor OWNER TO spade;

--
-- Name: poi; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE poi (
    pointofinterestkey integer NOT NULL,
    level integer,
    status integer,
    whenoccurred timestamp without time zone,
    guid_poiguidkey integer,
    type_poitypekey integer
);


ALTER TABLE public.poi OWNER TO spade;

--
-- Name: poiattribute; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE poiattribute (
    poiattributekey integer NOT NULL,
    value character varying(255),
    name_poiattributenamekey integer,
    pointofinterest_pointofinterestkey integer
);


ALTER TABLE public.poiattribute OWNER TO spade;

--
-- Name: poiattributename; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE poiattributename (
    poiattributenamekey integer NOT NULL,
    value character varying(255)
);


ALTER TABLE public.poiattributename OWNER TO spade;

--
-- Name: poiguid; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE poiguid (
    poiguidkey integer NOT NULL,
    value character varying(255)
);


ALTER TABLE public.poiguid OWNER TO spade;

--
-- Name: poitype; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE poitype (
    poitypekey integer NOT NULL,
    value character varying(255)
);


ALTER TABLE public.poitype OWNER TO spade;

--
-- Name: seenitem; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE seenitem (
    seenitemkey integer NOT NULL,
    item character varying(256) NOT NULL,
    lastwatched_lastwatchedkey integer NOT NULL
);


ALTER TABLE public.seenitem OWNER TO spade;

--
-- Name: slicedexecution; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE slicedexecution (
    slicedexecutionkey integer NOT NULL,
    bytes bigint NOT NULL,
    count integer NOT NULL,
    interval_intervalkey integer NOT NULL,
    topic_topickey integer NOT NULL
);


ALTER TABLE public.slicedexecution OWNER TO spade;

--
-- Name: slicedinterval; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE slicedinterval (
    intervalkey integer NOT NULL,
    duration bigint NOT NULL,
    whenended timestamp without time zone NOT NULL
);


ALTER TABLE public.slicedinterval OWNER TO spade;

--
-- Name: slicedtopic; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE slicedtopic (
    topickey integer NOT NULL,
    description character varying(255),
    "interval" bigint NOT NULL
);


ALTER TABLE public.slicedtopic OWNER TO spade;

--
-- Name: ticketedfile; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE ticketedfile (
    ticketedfilekey integer NOT NULL,
    archivesuffix character varying(255),
    binarysize bigint,
    bundle character varying(255) NOT NULL,
    checksum bigint,
    compressedsize bigint,
    metadatasize bigint,
    packagedsize bigint,
    placedsize bigint,
    placementid character varying(255),
    remotenumber integer,
    remoteregistrationid character varying(255),
    uuid character varying(255),
    whenabandoned timestamp without time zone,
    whenarchived timestamp without time zone,
    whenbinaryset timestamp without time zone,
    whencompleted timestamp without time zone,
    whencompressedset timestamp without time zone,
    whenflushed timestamp without time zone,
    whenmetadataset timestamp without time zone,
    whenpackagedset timestamp without time zone,
    whenplaced timestamp without time zone,
    whenrestored timestamp without time zone,
    whenticketed timestamp without time zone,
    whenverificationcompleted timestamp without time zone,
    whenverificationstarted timestamp without time zone,
    whenwrappedset timestamp without time zone,
    wrappedsize bigint,
    localregistration_registrationkey integer NOT NULL
);


ALTER TABLE public.ticketedfile OWNER TO spade;

--
-- Name: verified; Type: TABLE; Schema: public; Owner: spade; Tablespace: 
--

CREATE TABLE verified (
    verifiedkey integer NOT NULL,
    whenabandoned timestamp without time zone,
    whenverified timestamp without time zone,
    destination_neighborkey integer NOT NULL,
    ticketedfile_ticketedfilekey integer NOT NULL
);


ALTER TABLE public.verified OWNER TO spade;

--
-- Name: dispatch_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY dispatch
    ADD CONSTRAINT dispatch_pkey PRIMARY KEY (dispatchkey);


--
-- Name: entry_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY entry
    ADD CONSTRAINT entry_pkey PRIMARY KEY (entrykey);


--
-- Name: hierarchicalpath_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY hierarchicalpath
    ADD CONSTRAINT hierarchicalpath_pkey PRIMARY KEY (pathkey);


--
-- Name: inboundregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY inboundregistration
    ADD CONSTRAINT inboundregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: lastwatched_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY lastwatched
    ADD CONSTRAINT lastwatched_pkey PRIMARY KEY (lastwatchedkey);


--
-- Name: localregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY localregistration
    ADD CONSTRAINT localregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: neighbor_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY neighbor
    ADD CONSTRAINT neighbor_pkey PRIMARY KEY (neighborkey);


--
-- Name: poi_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (pointofinterestkey);


--
-- Name: poiattribute_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY poiattribute
    ADD CONSTRAINT poiattribute_pkey PRIMARY KEY (poiattributekey);


--
-- Name: poiattributename_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY poiattributename
    ADD CONSTRAINT poiattributename_pkey PRIMARY KEY (poiattributenamekey);


--
-- Name: poiguid_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY poiguid
    ADD CONSTRAINT poiguid_pkey PRIMARY KEY (poiguidkey);


--
-- Name: poitype_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY poitype
    ADD CONSTRAINT poitype_pkey PRIMARY KEY (poitypekey);


--
-- Name: seenitem_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY seenitem
    ADD CONSTRAINT seenitem_pkey PRIMARY KEY (seenitemkey);


--
-- Name: slicedexecution_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY slicedexecution
    ADD CONSTRAINT slicedexecution_pkey PRIMARY KEY (slicedexecutionkey);


--
-- Name: slicedinterval_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY slicedinterval
    ADD CONSTRAINT slicedinterval_pkey PRIMARY KEY (intervalkey);


--
-- Name: slicedtopic_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY slicedtopic
    ADD CONSTRAINT slicedtopic_pkey PRIMARY KEY (topickey);


--
-- Name: ticketedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY ticketedfile
    ADD CONSTRAINT ticketedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: verified_pkey; Type: CONSTRAINT; Schema: public; Owner: spade; Tablespace: 
--

ALTER TABLE ONLY verified
    ADD CONSTRAINT verified_pkey PRIMARY KEY (verifiedkey);


--
-- Name: fk_196qq757dcc1r2ieystsn0urc; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY entry
    ADD CONSTRAINT fk_196qq757dcc1r2ieystsn0urc FOREIGN KEY (compressedpath_pathkey) REFERENCES hierarchicalpath(pathkey);


--
-- Name: fk_53uk8ho1k2oubj0ium64sssj4; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY poiattribute
    ADD CONSTRAINT fk_53uk8ho1k2oubj0ium64sssj4 FOREIGN KEY (pointofinterest_pointofinterestkey) REFERENCES poi(pointofinterestkey);


--
-- Name: fk_56exsceguynw0a0xmncc86ift; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY poi
    ADD CONSTRAINT fk_56exsceguynw0a0xmncc86ift FOREIGN KEY (type_poitypekey) REFERENCES poitype(poitypekey);


--
-- Name: fk_68l6tca3cufhrcq8wjdh9kxyg; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY dispatch
    ADD CONSTRAINT fk_68l6tca3cufhrcq8wjdh9kxyg FOREIGN KEY (destination_neighborkey) REFERENCES neighbor(neighborkey);


--
-- Name: fk_7kee6ae2umhw4jro0ylw322wc; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY entry
    ADD CONSTRAINT fk_7kee6ae2umhw4jro0ylw322wc FOREIGN KEY (wrappedpath_pathkey) REFERENCES hierarchicalpath(pathkey);


--
-- Name: fk_c9qicwb6hfcq4pvkul56qd0bt; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY slicedexecution
    ADD CONSTRAINT fk_c9qicwb6hfcq4pvkul56qd0bt FOREIGN KEY (topic_topickey) REFERENCES slicedtopic(topickey);


--
-- Name: fk_dqcg5d4kdd57jxw3g7pgad48; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY poiattribute
    ADD CONSTRAINT fk_dqcg5d4kdd57jxw3g7pgad48 FOREIGN KEY (name_poiattributenamekey) REFERENCES poiattributename(poiattributenamekey);


--
-- Name: fk_iocdyb7j1jqef263126ppub3; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY slicedexecution
    ADD CONSTRAINT fk_iocdyb7j1jqef263126ppub3 FOREIGN KEY (interval_intervalkey) REFERENCES slicedinterval(intervalkey);


--
-- Name: fk_iu0c7brng4m5u1niyf2vw2lxr; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY dispatch
    ADD CONSTRAINT fk_iu0c7brng4m5u1niyf2vw2lxr FOREIGN KEY (ticketedfile_ticketedfilekey) REFERENCES ticketedfile(ticketedfilekey);


--
-- Name: fk_ixqwvl82ya21jhs3oolnj5a4b; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY entry
    ADD CONSTRAINT fk_ixqwvl82ya21jhs3oolnj5a4b FOREIGN KEY (metapath_pathkey) REFERENCES hierarchicalpath(pathkey);


--
-- Name: fk_klhh17dflgpgse45c7jja6f0p; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY entry
    ADD CONSTRAINT fk_klhh17dflgpgse45c7jja6f0p FOREIGN KEY (datapath_pathkey) REFERENCES hierarchicalpath(pathkey);


--
-- Name: fk_lp7hp17d5uswpg7f2lmx41gf5; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY hierarchicalpath
    ADD CONSTRAINT fk_lp7hp17d5uswpg7f2lmx41gf5 FOREIGN KEY (parent_pathkey) REFERENCES hierarchicalpath(pathkey);


--
-- Name: fk_ml0kbrdtd4j9hl3ca2ctb771r; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY verified
    ADD CONSTRAINT fk_ml0kbrdtd4j9hl3ca2ctb771r FOREIGN KEY (ticketedfile_ticketedfilekey) REFERENCES ticketedfile(ticketedfilekey);


--
-- Name: fk_mwwmcqb98ugj7mbnghs785jbh; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY inboundregistration
    ADD CONSTRAINT fk_mwwmcqb98ugj7mbnghs785jbh FOREIGN KEY (registrationkey) REFERENCES localregistration(registrationkey);


--
-- Name: fk_puer2g1l0r9rdkjb3lkbym9jb; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY seenitem
    ADD CONSTRAINT fk_puer2g1l0r9rdkjb3lkbym9jb FOREIGN KEY (lastwatched_lastwatchedkey) REFERENCES lastwatched(lastwatchedkey);


--
-- Name: fk_q0xq8hr9hadcxsa299m4m1or0; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY inboundregistration
    ADD CONSTRAINT fk_q0xq8hr9hadcxsa299m4m1or0 FOREIGN KEY (deliverer_neighborkey) REFERENCES neighbor(neighborkey);


--
-- Name: fk_q2yb4kymjo9xnblac8hhtyje; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY ticketedfile
    ADD CONSTRAINT fk_q2yb4kymjo9xnblac8hhtyje FOREIGN KEY (localregistration_registrationkey) REFERENCES localregistration(registrationkey);


--
-- Name: fk_rkicv27sihj2g7w4xqljtvnyp; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY poi
    ADD CONSTRAINT fk_rkicv27sihj2g7w4xqljtvnyp FOREIGN KEY (guid_poiguidkey) REFERENCES poiguid(poiguidkey);


--
-- Name: fk_s4v206j3b4197vftujc3y3efv; Type: FK CONSTRAINT; Schema: public; Owner: spade
--

ALTER TABLE ONLY verified
    ADD CONSTRAINT fk_s4v206j3b4197vftujc3y3efv FOREIGN KEY (destination_neighborkey) REFERENCES neighbor(neighborkey);


--
-- PostgreSQL database dump complete
--

