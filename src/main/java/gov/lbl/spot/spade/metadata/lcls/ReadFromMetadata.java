package gov.lbl.spot.spade.metadata.lcls;

import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.DropLocation;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * This class implements the {@link DataLocator} interface for the
 * {@link LclsMetadata} class.
 * 
 * @author patton
 */
public class ReadFromMetadata extends
                             DataLocator {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public Bundle createBundle(String name,
                               DropLocation dropLocation,
                               File file) {
        try {
            JAXBContext content = JAXBContext.newInstance(LclsMetadata.class);
            Unmarshaller unmarshaller = content.createUnmarshaller();
            final LclsMetadata metadata = (LclsMetadata) unmarshaller.unmarshal(file);
            return new Bundle(name,
                              normalizeLocation(dropLocation,
                                                metadata.getDataPath()));
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
