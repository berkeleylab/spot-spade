package gov.lbl.spot.spade.metadata.bl733;

import static org.junit.Assert.assertEquals;
import static utils.TestUtils.createTempFile;
import static utils.TestUtils.writeDefaultBL733Metadata;
import static utils.TestUtils.writeMetaToFile;
import static utils.TestUtils.writeTiledBL733Semaphore;
import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.Bundle.Element;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BL733DataLocatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	public static void addImageInfoToSet(ImageInfo imageInfo, Set<String> set) {
		set.add(imageInfo.getDataPath());
		set.add(imageInfo.getMetadataPath());
		// set.add(imageInfo.getCalibrationDataPath());
		// set.add(imageInfo.getCalibrationMetadataPath());
	}
	
	@Test
	public void testTiled() throws Exception {
		File def = createTempFile("defbl733", ".xml");
		File sem = createTempFile("bl733", ".sem");
		
		BL733Metadata defaults = writeDefaultBL733Metadata(def);
		BL733Metadata semaphore = writeTiledBL733Semaphore(sem);
		
		File metadataFile = createTempFile("bl733", ".xml");
		semaphore.merge(defaults);
		
		writeMetaToFile(semaphore, metadataFile);
		
	    BL733DataLocator dataLocator = new BL733DataLocator(null);
		
		Bundle bundle = dataLocator.createBundle("a_name", null, metadataFile);
		
		assertEquals(bundle.getName(), "a_name");
		assertEquals(bundle.isContainer(), true);
		assertEquals(bundle.getLocation(), null);
		
		TiledDetector det = (TiledDetector) semaphore.getDetectors().get(0);
		
		Set<String> set = new HashSet<String>();
		
		addImageInfoToSet(det.getHighImageInfo(), set);
		addImageInfoToSet(det.getLowImageInfo(), set);
		addImageInfoToSet(det.getStitchedImageInfo(), set);
		
		Set<String> eset = new HashSet<String>();
		
		Collection<Element> elements = bundle.getElements();
		
		for (Element element: elements) {
			eset.add(element.getFullLocation().substring("localhost:".length()));
		}
		
		assertEquals(eset, set);
	}

	
	public static void main(String[] args) throws Exception {
		new BL733DataLocatorTest().testTiled();
	}
}
