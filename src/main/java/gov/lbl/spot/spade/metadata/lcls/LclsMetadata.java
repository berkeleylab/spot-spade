package gov.lbl.spot.spade.metadata.lcls;

import gov.lbl.nest.spade.registry.Metadata;
import gov.lbl.spot.spade.metadata.SpotMetadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The is the {@link Metadata} implementation for LCLS experiments.
 * 
 * @author patton
 */
@XmlRootElement(name = "lcls-metadata")
@XmlType(propOrder = { "dataPath",
                      "experimentName" })
public class LclsMetadata extends
                         SpotMetadata {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The path to the original data.
     */
    private String dataPath;

    /**
     * The name of the experiment that took the file.
     */
    private String experimentName;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the path to the original data.
     * 
     * @return the path to the original data.
     */
    @XmlElement(name = "data_path")
    public String getDataPath() {
        return dataPath;
    }

    /**
     * Returns the name of the experiment that took the file.
     * 
     * @return the name of the experiment that took the file.
     */
    @XmlElement(name = "experiment_name")
    public String getExperimentName() {
        return experimentName;
    }

    @Override
    public void merge(SpotMetadata defaults) {
        super.merge(defaults);
        final LclsMetadata lclsMetadata = (LclsMetadata) defaults;
        if (null == getExperimentName()) {
            setExperimentName(lclsMetadata.getExperimentName());
        }
    }

    /**
     * Sets the path to the original data.
     * 
     * @param path
     *            the path to the original data.
     */
    protected void setDataPath(String path) {
        dataPath = path;
    }

    /**
     * Sets the name of the experiment that took the file.
     * 
     * @param name
     *            the name of the experiment that took the file.
     */
    protected void setExperimentName(String name) {
        experimentName = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
