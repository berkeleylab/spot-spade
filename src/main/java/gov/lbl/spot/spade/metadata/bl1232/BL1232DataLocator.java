package gov.lbl.spot.spade.metadata.bl1232;

import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DropLocation;
import gov.lbl.spot.spade.metadata.BLDataLocator;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

import java.io.File;
import java.io.IOException;

/**
 * 
 */
public class BL1232DataLocator extends
                              BLDataLocator {
    public BL1232DataLocator(File configDir) throws IOException {
        super(configDir);
    }

    private String construct(String dataset,
                             String ext,
                             int num) {
        StringBuilder sb = new StringBuilder();

        sb.append(dataset);
        sb.append("_");

        String number = Integer.toString(num);

        switch (number.length()) {
        case 1:
            sb.append("0000");
            break;
        case 2:
            sb.append("000");
            break;
        case 3:
            sb.append("00");
            break;
        case 4:
            sb.append("0");
            break;
        case 5:
            break;
        default:
            throw new RuntimeException("length =" + number.length());

        }

        sb.append(number);
        sb.append(".");
        sb.append(ext);
        return sb.toString();
    }

    @Override
    public Bundle createBundle(String name,
                               DropLocation dropLocation,
                               File file) {
        try {
            final SpotMetadata metadata = new SpotMetadataManager().createMetadata(file);

            if (metadata instanceof BL1232Metadata == false) {
                throw new Exception("Metata data read is not from BL832 ...");
            }

            final BL1232Metadata bl1232metadata = (BL1232Metadata) metadata;
            final Bundle result = new Bundle(name,
                                             true,
                                             ":");

            final File dirFile = new File(bl1232metadata.getDataPath());

            if (!dirFile.isDirectory()) {
                LOG.warn(dirFile.getAbsolutePath() + " does not exist");
                return null;
            }

            final int first = Integer.parseInt(bl1232metadata.getFirst());
            final int last = Integer.parseInt(bl1232metadata.getLast());

            if (first < 0) {
                LOG.warn("first < 0" + dirFile.getAbsolutePath()
                         + ":ds="
                         + bl1232metadata.getDataset()
                         + ":first="
                         + first
                         + ":last="
                         + last
                         + ":ext="
                         + bl1232metadata.getExt());
                return null;
            }

            if (first > last) {
                LOG.warn("first > last" + dirFile.getAbsolutePath()
                         + ":ds="
                         + bl1232metadata.getDataset()
                         + ":first="
                         + first
                         + ":last="
                         + last
                         + ":ext="
                         + bl1232metadata.getExt());
                return null;
            }

            for (int i = first; i <= last; i++) {
                File temp = new File(dirFile,
                                     construct(bl1232metadata.getDataset(),
                                               bl1232metadata.getExt(),
                                               i));

                if (temp.exists()) {
                    result.addElement(LOCALHOST + ":"
                                              + getLocalFile(temp.getAbsolutePath()),
                                      null);
                } else {
                    LOG.warn(temp.getAbsolutePath() + " does not exists therefore not added to bundle");
                }
            }

            if (result.getElements()
                .size() == 0) {
                LOG.warn("no images found in " + dirFile.getAbsolutePath()
                         + ":ds="
                         + bl1232metadata.getDataset()
                         + ":first="
                         + first
                         + ":last="
                         + last
                         + ":ext="
                         + bl1232metadata.getExt());
                return null;
            }

            return result;
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
