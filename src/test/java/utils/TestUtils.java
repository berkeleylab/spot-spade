package utils;

import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.bl1232.BL1232Metadata;
import gov.lbl.spot.spade.metadata.bl733.BL733Metadata;
import gov.lbl.spot.spade.metadata.bl733.BurstDetector;
import gov.lbl.spot.spade.metadata.bl733.Detector;
import gov.lbl.spot.spade.metadata.bl733.ImageInfo;
import gov.lbl.spot.spade.metadata.bl733.SingleDetector;
import gov.lbl.spot.spade.metadata.bl733.TiledDetector;
import gov.lbl.spot.spade.metadata.bl832.BL832Metadata;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class TestUtils {

	public static void delete(File file) throws IOException {
		if (file.isDirectory()) {
			if (file.list().length == 0) {
				file.delete();
			} else {
				File files[] = file.listFiles();

				for (File temp : files) {
					delete(temp);
				}

				if (file.list().length == 0) {
					file.delete();
				}
			}

		} else {
			file.delete();
		}
	}
	 
	public static boolean DELETE_ON_EXIT = false;

	public static File createTempFile(String name, String extension)
			throws IOException {
		File file = File.createTempFile(name, extension);

		if (DELETE_ON_EXIT) {
			file.deleteOnExit();
		}

		return file;
	}
	
	public static void writeMetaToFile(SpotMetadata metadata, File file) throws JAXBException {
		JAXBContext content = JAXBContext.newInstance(SpotMetadata.class);
		Marshaller marshaller = content.createMarshaller();
		
		marshaller.setProperty("jaxb.formatted.output", true);
		marshaller.marshal(metadata, file);
	}

	public static BL733Metadata writeDefaultBL733Metadata(File file)
			throws Exception {
		BL733Metadata metadata = new BL733Metadata();

		metadata.setFacility("als");
		metadata.setEndStation("bl733");
		metadata.setStage("raw");
		metadata.setStageFlow("/raw");
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public static BL733Metadata writeNoCalibrationBL733Semaphore(File file)
			throws Exception {
		BL733Metadata metadata = new BL733Metadata();

		metadata.setOwner("alex");

		ImageInfo bimageInfo = new ImageInfo("/b-images.log",
				"/b-image-meta.txt", "/b-calibration-image.edf",
				"/b-calibration-meta.txt");

		BurstDetector bdetector = new BurstDetector("100M", bimageInfo);

		ImageInfo simageInfo = new ImageInfo("/s-images.edf",
				"/s-image-meta.txt", null, null);

		SingleDetector sdetector = new SingleDetector("10K", simageInfo);

		ImageInfo limageInfo = new ImageInfo("/t-low-images.edf",
				"/t-low-image-meta.txt", null, null);
		ImageInfo himageInfo = new ImageInfo("/t-high-images.edf",
				"/t-high-image-meta.txt", null, null);
		ImageInfo ssimageInfo = new ImageInfo("/t-stitched-images.edf",
				"/t-stitched-image-meta.txt", null, null);
		TiledDetector tdetector = new TiledDetector("9999K", limageInfo,
				himageInfo, ssimageInfo);

		List<Detector> dets = new ArrayList<Detector>();

		dets.add(bdetector);
		dets.add(sdetector);
		dets.add(tdetector);

		metadata.setDataset("SomeDataSetName");
		metadata.setDetectors(dets);

		List<String> keywords = new ArrayList<String>();

		keywords.add("kw1");
		keywords.add("kw2");

		metadata.setKeywords(keywords);
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public static BL733Metadata writeNoStitchesBL733Semaphore(File file)
			throws Exception {
		BL733Metadata metadata = new BL733Metadata();

		metadata.setOwner("alex");

		ImageInfo bimageInfo = new ImageInfo("/b-images.log",
				"/b-image-meta.txt", "/b-calibration-image.edf",
				"/b-calibration-meta.txt");

		BurstDetector bdetector = new BurstDetector("100M", bimageInfo);

		ImageInfo simageInfo = new ImageInfo("/s-images.edf",
				"/s-image-meta.txt", "/s-calibration-image.edf",
				"/s-calibration-meta.txt");

		SingleDetector sdetector = new SingleDetector("10K", simageInfo);

		ImageInfo limageInfo = new ImageInfo("/t-low-images.edf",
				"/t-low-image-meta.txt", "/tlow-calibration-image.edf",
				"/t-low-calibration-meta.txt");
		ImageInfo himageInfo = new ImageInfo("/t-high-images.edf",
				"/t-high-image-meta.txt", "/thigh-calibration-image.edf",
				"/t-high-calibration-meta.txt");

		TiledDetector tdetector = new TiledDetector("9999K", limageInfo,
				himageInfo, null);

		List<Detector> dets = new ArrayList<Detector>();

		dets.add(bdetector);
		dets.add(sdetector);
		dets.add(tdetector);

		metadata.setDataset("SomeDataSetName");
		metadata.setDetectors(dets);

		List<String> keywords = new ArrayList<String>();

		keywords.add("kw1");
		keywords.add("kw2");

		metadata.setKeywords(keywords);
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public static BL733Metadata writeFullBL733Semaphore(File file)
			throws Exception {
		BL733Metadata metadata = new BL733Metadata();

		metadata.setOwner("alex");

		ImageInfo bimageInfo = new ImageInfo("/b-images.log",
				"/b-image-meta.txt", "/b-calibration-image.edf",
				"/b-calibration-meta.txt");

		BurstDetector bdetector = new BurstDetector("100M", bimageInfo);

		ImageInfo simageInfo = new ImageInfo("/s-images.edf",
				"/s-image-meta.txt", "/s-calibration-image.edf",
				"/s-calibration-meta.txt");

		SingleDetector sdetector = new SingleDetector("10K", simageInfo);

		ImageInfo limageInfo = new ImageInfo("/t-low-images.edf",
				"/t-low-image-meta.txt", "/tlow-calibration-image.edf",
				"/t-low-calibration-meta.txt");
		ImageInfo himageInfo = new ImageInfo("/t-high-images.edf",
				"/t-high-image-meta.txt", "/thigh-calibration-image.edf",
				"/t-high-calibration-meta.txt");
		ImageInfo ssimageInfo = new ImageInfo("/t-stitched-images.edf",
				"/t-stitched-image-meta.txt",
				"/t-stitched-calibration-image.edf",
				"/t-stitched-calibration-meta.txt");
		TiledDetector tdetector = new TiledDetector("9999K", limageInfo,
				himageInfo, ssimageInfo);

		List<Detector> dets = new ArrayList<Detector>();

		dets.add(bdetector);
		dets.add(sdetector);
		dets.add(tdetector);

		metadata.setDataset("SomeDataSetName");
		metadata.setDetectors(dets);

		List<String> keywords = new ArrayList<String>();

		keywords.add("kw1");
		keywords.add("kw2");

		metadata.setKeywords(keywords);
		writeMetaToFile(metadata, file);
		return metadata;
	}
	
	public static BL733Metadata writeTiledBL733Semaphore(File file)
			throws Exception {
		BL733Metadata metadata = new BL733Metadata();

		metadata.setOwner("alex");

		ImageInfo limageInfo = new ImageInfo("/t-low-images.edf",
				"/t-low-image-meta.txt", "/tlow-calibration-image.edf",
				"/t-low-calibration-meta.txt");
		ImageInfo himageInfo = new ImageInfo("/t-high-images.edf",
				"/t-high-image-meta.txt", "/thigh-calibration-image.edf",
				"/t-high-calibration-meta.txt");
		ImageInfo ssimageInfo = new ImageInfo("/t-stitched-images.edf",
				"/t-stitched-image-meta.txt",
				"/t-stitched-calibration-image.edf",
				"/t-stitched-calibration-meta.txt");
		TiledDetector tdetector = new TiledDetector("9999K", limageInfo,
				himageInfo, ssimageInfo);

		List<Detector> dets = new ArrayList<Detector>();

		dets.add(tdetector);

		metadata.setDataset("SomeDataSetName");
		metadata.setDetectors(dets);

		List<String> keywords = new ArrayList<String>();

		keywords.add("kw1");
		keywords.add("kw2");

		metadata.setKeywords(keywords);
		writeMetaToFile(metadata, file);
		return metadata;
	}
	

	public static BL832Metadata writeBL832Defaults(File file) throws Exception {
		BL832Metadata metadata = new BL832Metadata();

		metadata.setFacility("als");
		metadata.setEndStation("bl832");
		metadata.setStage("raw");
		metadata.setStageFlow("/raw");
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public static BL832Metadata writeBL832Semaphore(File file) throws Exception {
		BL832Metadata metadata = new BL832Metadata();

		metadata.setDataPath("/some/datapath");
		metadata.setOwner("dula");
		writeMetaToFile(metadata, file);
		return metadata;
	}
	
	public static BL1232Metadata writeBL1232Defaults(File file) throws Exception {
		BL1232Metadata metadata = new BL1232Metadata();

		metadata.setFacility("als");
		metadata.setEndStation("bl1232");
		metadata.setStage("raw");
		metadata.setStageFlow("/raw");
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public static BL1232Metadata writeBL1232Semaphore(File file) throws Exception {
		BL1232Metadata metadata = new BL1232Metadata();

		metadata.setDataPath("/Users/aessiari/spade4/spot-spade/src/test/resources/bl1232");
		metadata.setOwner("martin");
		metadata.setDataset("ds2");
		metadata.setExt("tif");
		metadata.setFirst("00001");
		metadata.setLast("00002");
		writeMetaToFile(metadata, file);
		return metadata;
	}

	public TestUtils() {
		super();
	}
}