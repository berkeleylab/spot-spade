package gov.lbl.spot.spade.metadata;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import gov.lbl.nest.spade.registry.Metadata;
import gov.lbl.nest.spade.registry.UtcPlacement;
import gov.lbl.spot.spade.metadata.aps.ApsMetadata;
import gov.lbl.spot.spade.metadata.bl1232.BL1232Metadata;
import gov.lbl.spot.spade.metadata.bl733.BL733Metadata;
import gov.lbl.spot.spade.metadata.bl832.BL832Metadata;
import gov.lbl.spot.spade.metadata.lcls.LclsMetadata;

/**
 * This is the base class from which all SPOT metadata classes should be
 * derived.
 * 
 * @author patton
 */
@XmlRootElement(name = "spot-metadata")
@XmlType(propOrder = { "facility",
                       "endStation",
                       "owner",
                       "dataset",
                       "stage",
                       "stageDateAndTime",
                       "stageFlow",
                       "uuid" })
@XmlSeeAlso({ ApsMetadata.class,
              BL832Metadata.class,
              BL733Metadata.class,
              BL1232Metadata.class,
              LclsMetadata.class })
public class SpotMetadata implements
                          Metadata,
                          UtcPlacement {
    /**
     * The end station, within this object's facility, that is associated with
     * this data.
     */
    private String endStation;
    /**
     * The facility that is associated with this data.
     */
    private String facility; // = "als";
    /**
     * The owner, operating at this object's end station, that is associated
     * with this data.
     */
    private String owner;
    /**
     * The owner defined dataset that is associated with this data.
     */
    private String dataset;
    /**
     * The processing stage that created this this data.
     */
    private String stage; // = "raw";
    /**
     * The date and time the associated stage completed this data.
     */
    private Date stageDateAndTime; // = new Date();
    /**
     * 
     */
    private String stageFlow; // = "/raw";
    /**
     * The uuid associated with this data.
     */
    private UUID uuid;

    public SpotMetadata() {
    }

    public String getDataset() {
        return dataset;
    }

    public void setDataset(String dataset) {
        this.dataset = dataset;
    }

    @XmlElement(name = "end_station")
    public String getEndStation() {
        return endStation;
    }

    public void setEndStation(String endStation) {
        this.endStation = endStation;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    @XmlElement(name = "stage_date")
    public Date getStageDateAndTime() {
        return stageDateAndTime;
    }

    public void setStageDateAndTime(Date stageDateAndTime) {
        this.stageDateAndTime = stageDateAndTime;
    }

    @XmlElement(name = "stage_flow")
    public String getStageFlow() {
        return stageFlow;
    }

    public void setStageFlow(String stageFlow) {
        this.stageFlow = stageFlow;
    }

    @XmlTransient
    public Date getUtcDateTime() {
        return stageDateAndTime;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Sets all <code>null</code> elements to their value in the supplied
     * defaults.
     * 
     * @param defaults
     *            the value of the elements to use when they are
     *            <code>null</code>.
     */
    public void merge(SpotMetadata defaults) {
        if (null == getEndStation()) {
            setEndStation(defaults.getEndStation());
        }
        if (null == getFacility()) {
            setFacility(defaults.getFacility());
        }
        if (null == getOwner()) {
            setOwner(defaults.getOwner());
        }
        if (null == getStageFlow()) {
            setStageFlow(defaults.getStageFlow());
        }
        if (null == getStage()) {
            setStage(defaults.getStage());
        }
        if (null == getStageDateAndTime()) {
            setStageDateAndTime(new Date());
        }
        if (null == getUuid()) {
            setUuid((UUID.randomUUID()));
        }
    }
}