package gov.lbl.spot.spade.metadata.bl832;

import gov.lbl.spot.spade.metadata.SpotMetadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "bl832-metadata")
@XmlType(propOrder = {
                      "dataPath"})

public class BL832Metadata extends SpotMetadata {
	private String dataPath;
	
	public BL832Metadata() {
		 // super.setEndStation("bl832");
	}
	
	/**
     * Returns the path to the original data.
     * 
     * @return the path to the original data.
     */
    @XmlElement(name = "data_path")
    public String getDataPath() {
        return dataPath;
    }

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}
}
