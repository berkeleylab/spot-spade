package gov.lbl.spot.spade.metadata.bl733;


import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlJavaTypeAdapter(DetectorAdapter.class)
public abstract class Detector {
	private final String name;
	
	public Detector(String name) {
		if (null == name) {
			throw new IllegalArgumentException();
		}
		
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
