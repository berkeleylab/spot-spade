package gov.lbl.spot.spade.policy;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.common.logging.Logger;
import gov.lbl.nest.common.logging.LoggerFactory;
import gov.lbl.nest.spade.policy.DuplicatingPolicy;
import gov.lbl.nest.spade.registry.ParseException;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

import java.io.File;
import java.io.IOException;

/**
 * This class does duplications for SPOT beamlines.
 * 
 * @author patton
 */
public class SpotDuplicating extends
                            LocalhostTransfer implements
                                             DuplicatingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.createLogger(SpotDuplicating.class);

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public String internalPath(String fileName,
                               File metadataFile) {
        SpotMetadata metadata;
        try {
            metadata = SpotMetadataManager.readMetaData(metadataFile);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Metadata can not be parsed",
                                               e);
        }
        final String owner = metadata.getOwner();
        if (null == owner) {
            throw new IllegalArgumentException("No owner specified in metadata");
        }
        return owner;
    }

    /**
     * THe method is executed after a target data file and semaphore have been
     * successfully sent.
     * 
     * @param targetFile
     *            the name of the file in the remote location.
     * @param targetSemaphore
     *            the name of the semaphore file in the remote location..
     * @throws IOException
     * @throws InterruptedException
     */
    private void postSend(File targetFile,
                          File targetSemaphore) throws IOException,
                                               InterruptedException {
        final String[] cmdArray = new String[] { "/usr/bin/sudo",
                                                "/usr/local/bin/fix_owner",
                                                targetFile.getCanonicalPath(),
                                                targetSemaphore.getCanonicalPath() };
        final Command command = new Command(cmdArray);
        command.execute();
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            final String[] stderr = cmdException.getStdErr();
            for (String line : stderr) {
                LOG.equals(line);
            }
            throw new IOException("Local \"" + cmdArray[0]
                                          + "\" execution failed",
                                  cmdException);
        }
    }

    @Override
    public void send(File file,
                     String targetFile,
                     File semaphore,
                     String targetSemaphore,
                     String location) throws IOException,
                                     InterruptedException {
        try {
            super.send(file,
                       targetFile,
                       semaphore,
                       targetSemaphore,
                       location);

            final String[] parts = location.split(":",
                                                  2);
            if (!LOCALHOST.equals(parts[0])) {
                throw new UnsupportedOperationException();
            }
            final String remoteFile;
            if (null == targetFile) {
                remoteFile = file.getName();
            } else {
                remoteFile = targetFile;
            }
            final String remoteSemaphore;
            if (null == targetSemaphore) {
                remoteSemaphore = semaphore.getName();
            } else {
                remoteSemaphore = targetSemaphore;
            }
            final File directory = new File(getResolvedPath(parts[1]));
            final File dataTarget = new File(directory,
                                             remoteFile);
            final File semaphoreTarget = new File(directory,
                                                  remoteSemaphore);
            postSend(dataTarget,
                     semaphoreTarget);

        } catch (IOException e) {
            throw e;
        } catch (InterruptedException e) {
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
