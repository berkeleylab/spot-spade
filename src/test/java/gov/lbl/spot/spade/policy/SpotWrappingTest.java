package gov.lbl.spot.spade.policy;

import gov.lbl.nest.spade.config.Policy;
import gov.lbl.nest.spade.policy.impl.LocalhostFetching;
import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DropLocation;
import gov.lbl.spot.spade.metadata.BLDataLocator;
import gov.lbl.spot.spade.metadata.bl1232.BL1232DataLocator;
import gov.lbl.spot.spade.metadata.bl733.BL733DataLocator;
import gov.lbl.spot.spade.metadata.bl832.BL832DataLocator;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utils.TestUtils;

public class SpotWrappingTest {
	private final File parent = new File("target/test-classes");
	private final File bl733InDir = new File(parent, "sample-bl733");
	private final File bl832InDir = new File(parent, "sample-bl832");
	private final File bl1232InDir = new File(parent, "sample-bl1232");
	
	private final File bl733MetadataFile = new File(parent, "sample-bl733.xml");
	private final File bl832MetadataFile = new File(parent, "sample-bl832.xml");
	private final File bl1232MetadataFile = new File(parent, "sample-bl1232.xml");
	
	
	private final File bl733WrappedFile = new File(parent, "sample-bl733.h5");
	private final File bl832WrappedFile = new File(parent, "sample-bl832.h5");
	private final File bl1232WrappedFile = new File(parent, "sample-bl1232.h5");
	

	
	private void cleanup() throws Exception{
		/* bl733WrappedFile.delete();
		bl832WrappedFile.delete();
		bl1232WrappedFile.delete();
		TestUtils.delete(bl733InDir);
		TestUtils.delete(bl832InDir);
		TestUtils.delete(bl1232InDir); */
	}
	
	@Before
	public void setUp() throws Exception {
		cleanup();
	}
		
		
	@After
	public void tearDown() throws Exception {
		cleanup();
	}

    @Test
	public void testBL733() throws Exception {
		final BL733DataLocator locator = new BL733DataLocator(null);
		final Bundle bundle = locator.createBundle("sample-bl733", null, bl733MetadataFile);
		final LocalhostFetching fetcher = new LocalhostFetching();
		
		fetcher.receive(bundle, bl733InDir);
		
		final Policy policy = new Policy() {
			@Override
			public String getParameter(final String key) {
				if (key.equals("wrap")) {
					return "java -jar target/test-classes/als-super-wrapper.jar -u {2} -f {3} -e {4} -o {5} -d {6} -s {7} -t {8} -x {0} {1}";
				}
				
				return null;
			}
		};
		
		final SpotWrapping wrapper = new SpotWrapping(policy);
		
		
		wrapper.wrap(bl733InDir, bl733MetadataFile, bl733WrappedFile);
		org.junit.Assert.assertTrue( bl733WrappedFile.isFile());
	}
	
	@Test
	public void testBL832() throws Exception {
		final BL832DataLocator locator = new BL832DataLocator();
		final DropLocation dropLocation = new DropLocation("localhost", new File("").getAbsolutePath());
		
		final Bundle bundle = locator.createBundle("sample-bl832", dropLocation, bl832MetadataFile);
		
		final LocalhostFetching fetcher = new LocalhostFetching();
		
		fetcher.receive(bundle, bl832InDir);
		
		final Policy policy = new Policy() {
			@Override
			public String getParameter(final String key) {
				if (key.equals("wrap")) {
					return "java -jar target/test-classes/als-super-wrapper.jar -u {2} -f {3} -e {4} -o {5} -d {6} -s {7} -t {8} -x {0} {1}";
				}
				
				return null;
			}
		};
		
		final SpotWrapping wrapper = new SpotWrapping(policy);
		
		
		wrapper.wrap(bl832InDir, bl832MetadataFile, bl832WrappedFile);
		org.junit.Assert.assertTrue( bl832WrappedFile.isFile());
	}
	
	@Test
	public void testBL1232() throws Exception {
		final BLDataLocator locator = new BL1232DataLocator(null);
		final DropLocation dropLocation = new DropLocation("localhost", new File("").getAbsolutePath());
		
		final Bundle bundle = locator.createBundle("sample-bl1232", dropLocation, bl1232MetadataFile);
		
		final LocalhostFetching fetcher = new LocalhostFetching();
		
		fetcher.receive(bundle, bl1232InDir);
		
		final Policy policy = new Policy() {
			@Override
			public String getParameter(final String key) {
				if (key.equals("wrap")) {
					return "java -jar target/test-classes/als-super-wrapper.jar -u {2} -f {3} -e {4} -o {5} -d {6} -s {7} -t {8} -x {0} {1}";
				}
				
				return null;
			}
		};
		
		final SpotWrapping wrapper = new SpotWrapping(policy);
		
		wrapper.wrap(bl1232InDir, bl1232MetadataFile, bl1232WrappedFile);
		org.junit.Assert.assertTrue( bl1232WrappedFile.isFile());
	}
	
	public static void main(final String[] args) throws Exception {
		new SpotWrappingTest().testBL1232();
	}
}
