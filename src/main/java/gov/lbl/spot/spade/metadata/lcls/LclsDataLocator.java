package gov.lbl.spot.spade.metadata.lcls;

import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.DropLocation;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

import java.io.File;

/**
 * This class implements the {@link DataLocator} interface for the
 * {@link SpotMetadata} class.
 * 
 * @author patton
 */
public class LclsDataLocator extends
                                 DataLocator {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public Bundle createBundle(String name,
                               DropLocation dropLocation,
                               File file) {
        try {
        	final SpotMetadata metadata = new SpotMetadataManager().createMetadata(file);
            
            if (metadata instanceof LclsMetadata == false) {
            	throw new Exception("Meta data read is not from LCLS ...");
            }
            
            return new Bundle(name,
                            normalizeLocation(dropLocation,
                                               ((LclsMetadata) metadata).getDataPath()));
        } catch (RuntimeException e) {
        	throw e;
        } catch (Exception e) {
			e.printStackTrace(System.err);
		    return null;
		}
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
