package gov.lbl.spot.spade.metadata.bl733;

import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DropLocation;
import gov.lbl.spot.spade.metadata.BLDataLocator;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 */
public class BL733DataLocator extends
                             BLDataLocator {
    /**
     * 
     */
    private static final String LOW = "low";

    /**
     * 
     */
    private static final String HIGH = "high";

    /**
     * 
     */
    private static final String STITCHED = "stitched";

    /**
     * 
     */
    private static final String CALIBRATION = "calibration";

    /**
     * 
     */
    private static final String LOG_DIR = "log";

    public BL733DataLocator(File configDir) throws IOException {
        super(configDir);
    }

    @Override
    public Bundle createBundle(final String name,
                               final DropLocation dropLocation,
                               final File metadataFile) {
        try {
            final SpotMetadata metadata = new SpotMetadataManager().createMetadata(metadataFile);

            if (metadata instanceof BL733Metadata == false) {
                throw new Exception("Metata data read is not an instance of BL733Metadata ..." + metadata.getClass());
            }

            final BL733Metadata bl733metadata = (BL733Metadata) metadata;
            final List<Detector> detectors = bl733metadata.getDetectors();

            if (null == detectors || detectors.isEmpty()) {
                throw new Exception("needs at least one single detector ...");
            }

            Set<String> names = new HashSet<String>();

            for (Detector detector : detectors) {
                names.add(detector.getName());
            }

            if (names.size() != detectors.size()) {
                throw new Exception("duplicate detectors ...");
            }

            final Bundle result = new Bundle(name,
                                             true,
                                             ":");

            for (Detector detector : detectors) {
                final File subdir = new File(detector.getName());

                if (detector instanceof SingleDetector) {
                    fillBundle(result,
                               (SingleDetector) detector,
                               subdir);
                } else if (detector instanceof TiledDetector) {
                    fillBundle(result,
                               (TiledDetector) detector,
                               subdir);
                } else if (detector instanceof BurstDetector) {
                    fillBundle(result,
                               (BurstDetector) detector,
                               subdir);
                } else {
                    throw new RuntimeException("unsupported detector type " + detector.getClass());
                }
            }
            return result;
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

    private void addImageInfo(final Bundle bundle,
                              final ImageInfo imageInfo,
                              final File subdir) {
        final String localDataPath = getLocalFile(imageInfo.getDataPath());
        if ((imageInfo.getDataPath()).endsWith("." + LOG_DIR)) {
            final File logDir = new File(subdir,
                                         LOG_DIR);

            bundle.addElement(LOCALHOST + ":"
                                      + localDataPath,
                              logDir);

            if (imageInfo.getMetadataPath() != null && ("true".equals(System.getProperty("JUNIT")) || // TODO
                                                                                                      // Fix
                                                                                                      // unit
                                                                                                      // test
                                                                                                      // ...
                new File(getLocalFile(imageInfo.getMetadataPath())).isFile())) {
                bundle.addElement(LOCALHOST + ":"
                                          + getLocalFile(imageInfo.getMetadataPath()),
                                  subdir);
            } else if (imageInfo.getMetadataPath() != null) { // TODO remove
                                                              // when Eric
                                                              // corrects this
                File wrongFile = new File(getLocalFile(imageInfo.getMetadataPath()));

                File dir = wrongFile.getParentFile();

                if (dir != null) {
                    String name = wrongFile.getName();
                    String ext = name.substring(name.lastIndexOf('.') + 1);

                    name = name.substring(0,
                                          name.lastIndexOf('.'));

                    File correctFile = new File(dir,
                                                name    + "_00000"
                                                        + "."
                                                        + ext);

                    if (correctFile.exists()) {
                        bundle.addElement(LOCALHOST + ":"
                                                  + correctFile.getAbsolutePath(),
                                          subdir);
                    }
                }
            }

        } else {
            bundle.addElement(LOCALHOST + ":"
                                      + localDataPath,
                              subdir);

            if (imageInfo.getMetadataPath() != null && ("true".equals(System.getProperty("JUNIT")) || // TODO
                new File(getLocalFile(imageInfo.getMetadataPath())).isFile())) {
                bundle.addElement(LOCALHOST + ":"
                                          + getLocalFile(imageInfo.getMetadataPath()),
                                  subdir);
            }
        }

        if (imageInfo.getCalibrationDataPath() != null && new File(getLocalFile(imageInfo.getCalibrationDataPath())).isFile()) {
            final File calibrationDir = new File(subdir,
                                                 CALIBRATION);

            bundle.addElement(LOCALHOST + ":"
                                      + getLocalFile(imageInfo.getCalibrationDataPath()),
                              calibrationDir);

            if (imageInfo.getCalibrationMetadataPath() != null && new File(getLocalFile(imageInfo.getCalibrationMetadataPath())).isFile()) {
                bundle.addElement(LOCALHOST + ":"
                                          + getLocalFile(imageInfo.getCalibrationMetadataPath()),
                                  calibrationDir);
            }
        }
    }

    private void fillBundle(Bundle bundle,
                            BurstDetector burst,
                            File subdir) throws IOException {
        final ImageInfo imageInfo = burst.getImageInfo();
        final String logFile = getLocalFile(imageInfo.getDataPath());
        final BufferedReader reader = new BufferedReader(new FileReader(logFile));

        try {
            String line = reader.readLine();

            while (null != line) {
                final String[] tokensStrings = line.split("\\s+");

                if (4 == tokensStrings.length /*
                                               * &&
                                               * tokensStrings[3].startsWith("/"
                                               * )
                                               */) {
                    final String imagePath = getLocalFile(tokensStrings[3]);

                    bundle.addElement(LOCALHOST + ":"
                                              + imagePath,
                                      subdir);
                }

                line = reader.readLine();
            }
        } finally {
            try {
                reader.close();
            } catch (IOException ioe) {
            }
        }

        addImageInfo(bundle,
                     imageInfo,
                     subdir);
    }

    private void fillBundle(Bundle bundle,
                            SingleDetector single,
                            File subdir) {
        final ImageInfo imageInfo = single.getImageInfo();

        addImageInfo(bundle,
                     imageInfo,
                     subdir);
    }

    private void fillBundle(Bundle bundle,
                            TiledDetector tiled,
                            File subdir) {
        final ImageInfo lowImageInfo = tiled.getLowImageInfo();
        final File lowSubdir = new File(subdir,
                                        LOW);

        addImageInfo(bundle,
                     lowImageInfo,
                     lowSubdir);

        final ImageInfo highImageInfo = tiled.getHighImageInfo();
        final File highSubdir = new File(subdir,
                                         HIGH);

        addImageInfo(bundle,
                     highImageInfo,
                     highSubdir);

        final ImageInfo stitchedImageInfo = tiled.getStitchedImageInfo();

        if (stitchedImageInfo != null) {
            final File stitchedSubdir = new File(subdir,
                                                 STITCHED);

            addImageInfo(bundle,
                         stitchedImageInfo,
                         stitchedSubdir);
        }
    }
}
