package gov.lbl.spot.spade.metadata;

import gov.lbl.nest.spade.registry.DataLocator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The class implements the basic functionality used by {@link DataLocator}
 * implementations
 * 
 * @author patton
 */
public abstract class BLDataLocator extends
                                   DataLocator {

    /**
     * The name of the properties file contains the mail settings
     */
    protected static final String MOUNT_POINT_PROPERTIES = "mount-point.properties";
    /**
     * The name used to access the host on which the application is running.
     */
    protected static final String LOCALHOST = "localhost";
    /**
     * A String with no content.
     */
    private static final String EMPTY_STRING = "";
    /**
     * The {@link Properties} defining the mapping between DAQ and local mount
     * points.
     */
    protected Properties mountPoints;

    /**
     * The {@link Logger} used by this class.
     */
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    protected BLDataLocator(File configDir) {
        super(configDir);
    }

    /**
     * Returns the effective mount point of the supplied DAQ path.
     * 
     * @param path
     *            the file path on the DAQ systems.
     * @return the effective mount point of the supplied DAQ path.
     */
    private String getDaqMountPoint(String path) {
        final Set<String> daqMounts = getMountPoints().stringPropertyNames();
        String result = EMPTY_STRING;
        for (String daqMount : daqMounts) {
            if (path.startsWith(daqMount) && daqMount.length() > result.length()) {
                result = daqMount;
            }
        }
        if (EMPTY_STRING.equals(result)) {
            return null;
        }
        return result;
    }

    /**
     * Returns the local file based on the DAQ file supplied.
     * 
     * @param file
     *            the DAQ file whose local file is being requested.
     * @return the local file based on the DAQ file supplied.
     * @throws FileNotFoundException
     *             when the local file does not exist.
     */
    protected String getLocalFile(String path) {
        if (getMountPoints().isEmpty()) {
            return path;
        }

        final File file = new File(path);
        final String parentPath = file.getParent();

        final String daqMountPoint = getDaqMountPoint(parentPath);

        if (null == daqMountPoint) {
            return path;
        }

        final String localMountPoint = getLocalMountPoint(daqMountPoint);
        final File result = new File(parentPath.replaceFirst(daqMountPoint,
                                                             localMountPoint),
                                     file.getName());
        if (!result.exists()) {
            LOG.warn("DAQ file \"" + result.getPath()
                     + "\" does not exist.");
        }
        return result.getPath();
    }

    /**
     * Returns the local mount point of the supplied DAQ mount point.
     * 
     * @param daqPoint
     *            the DAQ mount point to map.
     * @return the local mount point of the supplied DAQ mount point.
     */
    private String getLocalMountPoint(String daqPoint) {
        return getMountPoints().getProperty(daqPoint);
    }

    /**
     * Returns the {@link Properties} defining the mapping between DAQ and local
     * mount points.
     * 
     * @return the {@link Properties} defining the mapping between DAQ and local
     *         mount points.
     */
    private Properties getMountPoints() {
        if (null == mountPoints) {
            mountPoints = new Properties();
            final File mountPointProperties = new File(getConfigurationDirectory(),
                                                       MOUNT_POINT_PROPERTIES);

            if (!mountPointProperties.exists()) {
                LOG.info(mountPointProperties.getAbsolutePath() + " does not exist");
            } else {
                LOG.info("Loading mount properties from \"" + mountPointProperties.getAbsolutePath()
                         + "\"");

                InputStream is = null;
                try {
                    is = new FileInputStream(mountPointProperties);

                    mountPoints.load(is);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (null != is) {
                            is.close();
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }

                final Set<String> names = mountPoints.stringPropertyNames();

                if (names.isEmpty()) {
                    LOG.info("No mount points were specified for this object");
                } else {
                    LOG.info("The following mount points have been specified");
                    for (String name : names) {
                        LOG.info("  " + name
                                 + " = "
                                 + mountPoints.getProperty(name));
                    }
                }
            }
        }
        return mountPoints;
    }

}