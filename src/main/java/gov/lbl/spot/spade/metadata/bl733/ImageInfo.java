package gov.lbl.spot.spade.metadata.bl733;


public final class ImageInfo {

	private String dataPath;
	private String metadataPath;
	private String calibrationDataPath;
	private String calibrationMetadataPath;
	
	public ImageInfo(String dataPath, String metadataPath, String calibrationDataPath, String calibrationMetadataPath) {
		if (null == dataPath || null == metadataPath) {
			throw new IllegalArgumentException();
		}
		
		this.dataPath = dataPath.trim();
		this.metadataPath = metadataPath.trim();
		
		if (null != calibrationDataPath) {
			this.calibrationDataPath = calibrationDataPath.trim();
		}
		
		if (null !=  calibrationMetadataPath) {
			this.calibrationMetadataPath = calibrationMetadataPath.trim();
		}
	}

	public String getDataPath() {
		return dataPath != null && dataPath.length() != 0? dataPath: null;
	}
	
	public String getMetadataPath() {
		return metadataPath != null && metadataPath.length() != 0? metadataPath : null;
	}
	
	public String getCalibrationDataPath() {
		return calibrationDataPath != null && calibrationDataPath.length() != 0? calibrationDataPath : null;
	}

	public String getCalibrationMetadataPath() {
		return calibrationMetadataPath != null && calibrationMetadataPath.length() != 0? calibrationMetadataPath : null;
	}
}
