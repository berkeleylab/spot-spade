package gov.lbl.spot.spade.metadata.bl832;

import static org.junit.Assert.assertEquals;
import static utils.TestUtils.createTempFile;
import static utils.TestUtils.writeMetaToFile;
import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DropLocation;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utils.TestUtils;

public class BL832DataLocatorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() throws Exception {
		final File def = createTempFile("defbl832", ".xml");
		final File sem = createTempFile("bl832", ".sem");
		
		final BL832Metadata defaults = TestUtils.writeBL832Defaults(def);
		final BL832Metadata semaphore = TestUtils.writeBL832Semaphore(sem);
		
		final File metadataFile = createTempFile("bl832", ".xml");
		
		semaphore.merge(defaults);
		
		writeMetaToFile(semaphore, metadataFile);
		
	    final BL832DataLocator dataLocator = new BL832DataLocator();
	    final DropLocation dropLocation = new DropLocation("localhost", null);
		final Bundle bundle = dataLocator.createBundle("b_name", dropLocation, metadataFile);
		
		assertEquals(bundle.getName(), "b_name");
		assertEquals(bundle.isContainer(), false);
		assertEquals(bundle.getLocation(), dropLocation.getHost() + ":" + semaphore.getDataPath());
	}
	
	public static void main(final String[] args) throws Exception {
		new BL832DataLocatorTest().test();
	}

}
