package gov.lbl.spot.spade.metadata.bl1232;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import gov.lbl.nest.spade.policy.impl.LocalhostFetching;
import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.Bundle.Element;
import gov.lbl.spot.spade.metadata.BLDataLocator;

import java.io.File;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BL1232DataLocatorTest {
	private final File parent = new File("target/test-classes");

	private final File bl1232InDir = new File(parent, "sample-bl1232");
	private final File bl1232MetadataFile = new File(parent, "sample-bl1232.xml");
	
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void test() throws Exception {
		BLDataLocator dataLocator = new BL1232DataLocator(null);
		Bundle bundle = dataLocator.createBundle("a_name", null,  bl1232MetadataFile);
		Collection<Element> elements = bundle.getElements();
		
		assertEquals(elements.size(), 2);
		
		for (Element element: elements) {
			assertNotNull(element.getFullLocation());
			assertNull(element.getWithinBundle());
		}
		
		Set<String> eset = new HashSet<String>();
		
		for (Element element: elements) {
			assertTrue(element.getFullLocation().startsWith("localhost:"));
			eset.add(element.getFullLocation().substring("localhost:".length()));
			assertTrue(new File(element.getFullLocation().substring("localhost:".length())).isFile());
		}
		

		final LocalhostFetching fetcher = new LocalhostFetching();
		
		fetcher.receive(bundle, bl1232InDir);
	}
	
	public static void main(String[] args) throws Exception {
		new BL1232DataLocatorTest().test();
	}

}
