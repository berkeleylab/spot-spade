package gov.lbl.spot.spade.metadata.bl832;

import java.io.File;

import gov.lbl.nest.spade.registry.Bundle;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.DropLocation;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

/**
 * This class implements the {@link DataLocator} interface for the
 * {@link SpotMetadata} class.
 * 
 * @author patton
 */
public class BL832DataLocator extends
                              DataLocator {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The suffix used on pre-wrapped HDF5 files.
     */
    private static final String HDF5_SUFFIX = ".h5";

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public Bundle createBundle(String name,
                               DropLocation dropLocation,
                               File file) {
        try {
            final SpotMetadata metadata = new SpotMetadataManager().createMetadata(file);

            if (metadata instanceof BL832Metadata == false) {
                throw new Exception("Metadata read is not from BL832 ...");
            }
            final String dataPath = ((BL832Metadata) metadata).getDataPath();
            final String dataPathToUse;
            if (dataPath.endsWith(HDF5_SUFFIX)) {
                dataPathToUse = dataPath.substring(0,
                                                   dataPath.length() - HDF5_SUFFIX.length());
            } else {
                dataPathToUse = dataPath;
            }
            return new Bundle(name,
                              normalizeLocation(dropLocation,
                                                dataPathToUse));
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
