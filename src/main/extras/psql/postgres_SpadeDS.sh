#
# postgres_spadeDB.sh
#
# Sets the SpadeDS to be in a postreSQL server.
#

cd ${HOME}/server/wildfly-9.0.2.Final
cp -rp standalone/configuration/standalone.xml standalone/configuration/standalone.xml.`date +%Y-%m-%d`
cat > postgres_SpadeDS.txt << EOF 
*** standalone/configuration/standalone.xml.cp.3	1969-12-31 16:00:00.000000000 -0800
--- standalone/configuration/standalone.xml	        1969-12-31 16:00:00.000000000 -0800
***************
*** 149,154 ****
--- 149,168 ----
                          <user-name>sa</user-name>
                      </security>
                  </datasource>
+                 <datasource jndi-name="java:jboss/datasources/SpadeDS" pool-name="SpadeDS" enabled="true" use-java-context="true">
+                     <connection-url>jdbc:postgresql://localhost:5432/spade</connection-url>
+                     <driver>postgresql-9.2-1004.jdbc4.jar</driver>
+                     <security>
+                         <user-name>spade</user-name>
+                         <password>d@ta3xchange</password>
+                     </security>
+                     <validation>
+                         <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
+                         <validate-on-match>true</validate-on-match>
+                         <background-validation>false</background-validation>
+                         <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
+                     </validation>
+                 </datasource>
                  <drivers>
                      <driver name="h2" module="com.h2database.h2">
                          <xa-datasource-class>org.h2.jdbcx.JdbcDataSource</xa-datasource-class>
EOF
patch standalone/configuration/standalone.xml postgres_SpadeDS.txt
rm postgres_SpadeDS.txt
cd -
