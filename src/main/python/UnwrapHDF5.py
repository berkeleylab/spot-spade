#!/usr/bin/env python
"""
Unwraps ALS HFD5 files for SPADE
"""

import h5py
import os

HDF5_SUFFIX='.h5'

def outputMetadata(path,
                   owner):
    f = open(path,
             'w')
    if None == owner:
        element = ''
    else:
        element = '    <owner>' + owner + '</owner>\n'
    f.write("""<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<metadata>
""" + element + """</metadata>
""")


def unwrap(path,
           directory,
           metaSuffix,
           topOnly):
    if not path.endswith(HDF5_SUFFIX):
        pass
    if not os.path.exists(directory):
        os.makedirs(directory)
    name = os.path.basename(path)
    base = name[:-1 * len(HDF5_SUFFIX)]
    os.makedirs(os.path.join(directory,
                             base))

    f = h5py.File(path,
                  'r') 
    groups=list(f)
    list_of_attrs = list(f.attrs)
    metadata = os.path.join(directory,
                            base + metaSuffix)
    outputMetadata(metadata,
                   f.attrs["owner"])


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Unwraps ALS HFD5 files for SPADE')
    parser.add_argument('-m',
                        dest='metaSuffix',
                        default='.meta.xml',
                        help='the suffix to use for the unwrapped metadata file')
    parser.add_argument('-t',
                        action='store_true',
                        dest='topOnly',
                        default=False,
                        help='the top directory is created but one data is unwrapped')
    parser.add_argument('file',
                        help='The path to the HDF5 file to unwrap')
    parser.add_argument('directory',
                        help='The path to the directory into which to store the unwarpped files')

    args = parser.parse_args()
    unwrap(args.file,
           args.directory,
           args.metaSuffix,
           args.topOnly)
