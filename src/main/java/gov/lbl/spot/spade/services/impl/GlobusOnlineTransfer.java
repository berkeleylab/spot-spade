package gov.lbl.spot.spade.services.impl;

import gov.lbl.nest.globus.CommandGlobus;
import gov.lbl.nest.globus.json.resources.TASK;
import gov.lbl.nest.spade.services.FileTransfer;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;
import org.slf4j.Logger;

/**
 * This class implements the {@link FileTransfer} interface in order to transfer
 * files using 'Globus Online'.
 * 
 * @author patton
 */
public class GlobusOnlineTransfer implements
                                 FileTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The string indicating the files are directly accessible on the local file
     * system.
     */
    private static final String LOCALHOST = "localhost";

    // private static member data

    // private instance member data

    /**
     * The base directory where to find any configurations.
     */
    private File configDir;

    /**
     * The globus online endpoint for this object.
     */
    private String endpoint;

    /**
     * The {@link Logger} instance used by this object.
     */
    private Logger logger;

    // constructors

    // instance member method (alphabetic)

    @Override
    public boolean isAsynchonous() {
        return false;
    }

    @Override
    public void send(final File file,
                     String targetFile,
                     final File semaphore,
                     String targetSemaphore,
                     final String location) throws IOException,
                                           InterruptedException {
        endpoint = "olcf#dtn";

        executeTransfer(endpoint,
                        file,
                        targetFile,
                        location,
                        logger);
        try {
            executeTransfer(endpoint,
                            semaphore,
                            targetSemaphore,
                            location,
                            logger);
        } catch (IOException e) {
            // TODO remove remote 'localFile' copy if possible.
            throw e;
        }
    }

    @Override
    public void setLogger(final Logger logger) {
        this.logger = logger;
    }

    @Override
    public void setConfigDirectory(File directory) {
        configDir = directory;
        logger.info("Configuration directory set to " + configDir);
    }

    // static member methods (alphabetic)

    /**
     * Executes an 'scp' copy of the specified file.
     * 
     * @param endpoint
     *            TODO
     * @param file
     *            the local file to copy.
     * @param targetFile
     *            the name of the file in the remote location if different from
     *            the local name. <code>null</code> means that the files both
     *            have the same name.
     * @param targetLocation
     *            the target directory into which the file should be placed.
     * @param logger
     *            the {@link Logger} used to log transfers.
     * 
     * @throws IOException
     * @throws InterruptedException
     */
    private static void executeTransfer(String endpoint,
                                        final File file,
                                        String targetFile,
                                        final String targetLocation,
                                        final Logger logger) throws IOException,
                                                            InterruptedException {
        final String[] parts = targetLocation.split(":",
                                                    2);
        final String remoteName;
        if (null == targetFile) {
            remoteName = file.getName();
        } else {
            remoteName = targetFile;
        }
        if (LOCALHOST.equals(parts[0])) {
            logger.info("args" + file.toString()
                        + ", "
                        + new File(parts[1],
                                   remoteName).toString());
            LocalhostTransfer.copy(file,
                                   new File(parts[1],
                                            remoteName));
        } else {
            final CommandGlobus command = new CommandGlobus();
            final String submissionId = command.getSubmissionId();
            logger.info("s=" + submissionId
                        + " from \""
                        + endpoint
                        + ":"
                        + file.getCanonicalPath()
                        + "\" to \""
                        + targetLocation
                        + '/'
                        + remoteName
                        + "\"");
            final JSONObject link = command.startTransfer(submissionId,
                                                          endpoint + ":"
                                                                  + file.getCanonicalPath(),
                                                          targetLocation + '/'
                                                                  + remoteName);

            long wait = 2000L;
            TASK.Status status = TASK.Status.ACTIVE;
            status = command.getTaskStatus(link);
            while (TASK.Status.ACTIVE == status) {
                if (30000L > wait) {
                    wait *= 2;
                    logger.debug("Wait set to " + TimeUnit.SECONDS.convert(wait,
                                                                           TimeUnit.MILLISECONDS)
                                 + " seconds");
                }
                Thread.sleep(wait);
                status = command.getTaskStatus(link);
            }
            if (TASK.Status.SUCCEEDED != status) {
                throw new IOException("Globus transfer did not succeed");
            }
        }
    }
    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
