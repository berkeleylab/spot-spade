package gov.lbl.spot.spade.metadata.bl733;


public class SingleDetector extends Detector {

	private final ImageInfo imageInfo;

	public SingleDetector(String name, ImageInfo imageInfo) {
		super(name);
		
		if (null == imageInfo) {
			throw new IllegalArgumentException();
		}
		
		this.imageInfo = imageInfo;
	}
	
	public ImageInfo getImageInfo() {
		return imageInfo;
	}
}
