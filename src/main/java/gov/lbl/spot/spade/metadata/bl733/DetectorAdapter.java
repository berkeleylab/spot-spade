package gov.lbl.spot.spade.metadata.bl733;

import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class DetectorAdapter extends XmlAdapter<DetectorAdapter.AdaptedDetector, Detector> {
	
	@XmlType(propOrder = { "log",
	        "image",
	        "metadata",
	        "calibration_image",
	        "calibration_metadata"
	        })
	public static class AdaptedImageInfo {
		public AdaptedImageInfo() {
		}
		
		public AdaptedImageInfo(ImageInfo imageInfo, boolean isLog) {
			if (isLog) {
				this.log = imageInfo.getDataPath();
			} else {
				this.image =  imageInfo.getDataPath(); 
			}
			
			this.metadata = imageInfo.getMetadataPath();
			this.calibration_image = imageInfo.getCalibrationDataPath();
			this.calibration_metadata = imageInfo.getCalibrationMetadataPath();
		}
		
		public ImageInfo toImageInfo(boolean isLog) {
			return new ImageInfo(isLog? log : image, metadata, calibration_image, calibration_metadata);
		}
		
		public String log;
		public String image;
		public String metadata;
		public String calibration_image;
		public String calibration_metadata;
	}
	
	public static class AdaptedDetector {
		public static class Tiled {
			public AdaptedImageInfo low;
			
			public AdaptedImageInfo high;
			
			public AdaptedImageInfo stitched;
		}
		

		public String name;
		
		public AdaptedImageInfo single;
		
		public AdaptedImageInfo burst;
		
		public Tiled tiled;
	}

	@Override
	public AdaptedDetector marshal(Detector detector) throws Exception {
		AdaptedDetector ret = new AdaptedDetector();
		
		ret.name = detector.getName();
		
		if (detector instanceof BurstDetector) {
			BurstDetector arg = (BurstDetector) detector;
			
			ret.burst= new AdaptedImageInfo(arg.getImageInfo(), true);
		} else if (detector instanceof SingleDetector) {
			SingleDetector arg = (SingleDetector) detector;
			
			
			ret.single = new AdaptedImageInfo(arg.getImageInfo(), false);
		} else if (detector instanceof TiledDetector) {
			TiledDetector arg = (TiledDetector) detector;
			
			ret.tiled = new AdaptedDetector.Tiled();
			ret.tiled.low = new AdaptedImageInfo(arg.getLowImageInfo(), false);
			ret.tiled.high = new AdaptedImageInfo(arg.getHighImageInfo(), false);
			ret.tiled.stitched = null == arg.getStitchedImageInfo()? null : new AdaptedImageInfo(arg.getStitchedImageInfo(), false);
		} else {
			throw new Exception("Unkown detector type " + detector.getClass());
		}
		
		return ret;
	}

	@Override
	public Detector unmarshal(AdaptedDetector v) throws Exception {
		Detector detector = null;
		
		if (v.single != null) {
			detector = new SingleDetector(v.name, v.single.toImageInfo(false));
		} else if (v.burst != null) {
			detector = new BurstDetector(v.name, v.burst.toImageInfo(true));
		} else if (v.tiled != null){
			detector = new TiledDetector(v.name, v.tiled.low.toImageInfo(false), 
					v.tiled.high.toImageInfo(false),
					null == v.tiled.stitched? null: v.tiled.stitched.toImageInfo(false));
		} else {
			throw new Exception("Detector missing image info ...");
		}
	
		
		return detector;
	}
}
