package gov.lbl.spot.spade.metadata.bl1232;

import gov.lbl.spot.spade.metadata.SpotMetadata;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "bl1232-metadata")
@XmlType(propOrder = {
                      "dataPath",
                      "ext",
                      "first",
                      "last",
                      "total"})

public class BL1232Metadata extends SpotMetadata {
	private String dataPath;
	
    private String ext;
    
    private String first;
    
    private String last;
    
    private String total;

    
	public BL1232Metadata() {
		 // super.setEndStation("bl1232");
	}
	
	/**
     * Returns the path to the original data.
     * 
     * @return the path to the original data.
     */
    @XmlElement(name = "data_path")
    public String getDataPath() {
        return dataPath;
    }

	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
