package gov.lbl.spot.spade.warehouse;

import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import gov.lbl.nest.spade.warehouse.Location;
import gov.lbl.spot.spade.execute.ResourcesForSpotSpade;

public class WarehouseAccess {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * Allows for simple testing from the command line.
     * 
     * @param args
     *            the arguments provided on the command line.
     * @throws JAXBException
     *             if the configuration file can not be parsed.
     */
    public static void main(String args[]) throws JAXBException {
        final ResourcesForSpotSpade resources = new ResourcesForSpotSpade();
        final SpotWarehouse warehouse = new SpotWarehouse(resources.getMongoDb());
        final Location location = warehouse.getLocation(args[0]);

        PrintWriter writer = new PrintWriter(System.out);
        JAXBContext content = JAXBContext.newInstance(Location.class);
        Marshaller marshaller = content.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                               true);
        marshaller.marshal(location,
                           writer);
    }
}
