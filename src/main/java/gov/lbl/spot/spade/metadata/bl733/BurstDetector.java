package gov.lbl.spot.spade.metadata.bl733;

public final class BurstDetector extends Detector {
	
	private final ImageInfo imageInfo;

	public BurstDetector(String name, ImageInfo imageInfo) {
		super(name);
		
		if (null == imageInfo) {
			throw new IllegalArgumentException();
		}
		
		this.imageInfo = imageInfo;
	}
	
	public ImageInfo getImageInfo() {
		return imageInfo;
	}
}
