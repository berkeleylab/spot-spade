package gov.lbl.spot.spade.metadata.bl733;


public final class TiledDetector extends Detector {
	private final ImageInfo lowImageInfo;
	
	private final ImageInfo highImageInfo;
	
	private final ImageInfo stitchedImageInfo;
	
	public TiledDetector(String name, ImageInfo lowImageInfo, ImageInfo highImageInfo, ImageInfo stitchedImageInfo) {
		super(name);
		
		if (null == lowImageInfo  || null == highImageInfo) {
			throw new IllegalArgumentException();
		}
		
		this.lowImageInfo = lowImageInfo;
		this.highImageInfo = highImageInfo;
		this.stitchedImageInfo = stitchedImageInfo;
	}

	public ImageInfo getLowImageInfo() {
		return lowImageInfo;
	}

	public ImageInfo getHighImageInfo() {
		return highImageInfo;
	}

	public ImageInfo getStitchedImageInfo() {
		return stitchedImageInfo;
	}
}
