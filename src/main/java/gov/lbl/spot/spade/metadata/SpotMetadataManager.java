package gov.lbl.spot.spade.metadata;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.annotation.Priority;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.Metadata;
import gov.lbl.nest.spade.registry.ParseException;
import gov.lbl.nest.spade.services.MetadataManager;
import gov.lbl.nest.spade.services.impl.MetadataManagerImpl;

/**
 * This class implements the {@link MetadataManager} interface for ALS HDF5
 * files.
 * 
 * @author patton
 */
@Stateless
@Alternative
@Priority(value = 0)
public class SpotMetadataManager extends
                                 MetadataManagerImpl implements
                                 MetadataManager {

    // public static final member data

    /**
     * The {@link Logger} instance used by this class.
     */
    public final Logger LOG = LoggerFactory.getLogger(this.getClass());

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The suffix used for HDF5 files.
     */
    private static final String HDF5_SUFFIX = ".h5";

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    // constructors

    // instance member method (alphabetic)

    @Override
    protected Object convertFromMetadata(Metadata metadata) {
        return metadata;
    }

    @Override
    public SpotMetadata createMetadata(File file) throws ParseException {
        return readMetaData(file);
    }

    private static void writeToFile(SpotMetadata metadata,
                                    File file) throws Exception {
        JAXBContext content = JAXBContext.newInstance(metadata.getClass());
        Marshaller marshaller = content.createMarshaller();

        marshaller.setProperty("jaxb.formatted.output",
                               true);
        marshaller.marshal(metadata,
                           file);
    }

    @Override
    public SpotMetadata createMetadata(LocalRegistration registration,
                                       File dataFile,
                                       File semaphoreFile) throws FileNotFoundException,
                                                           IOException {
        try {
            final SpotMetadata result = readMetaData(semaphoreFile);
            final String defaultMetadata = registration.getDefaultMetadata();

            if (null != defaultMetadata) {
                SpotMetadata defaults = readMetaData(new File(getConfiguration().getDirectory(),
                                                              defaultMetadata));

                if ((defaults.getClass()).isInstance(result) == false) {
                    throw new ParseException("default metadata is not same as metadata " + result
                                             + " vs "
                                             + defaults.getClass());
                }

                result.merge(defaults);
            }

            if (null == result.getDataset()) {
                result.setDataset(defaultDataset(dataFile,
                                                 result));
            }

            return result;
        } catch (Exception e) {
            throw new IOException(e);
        }
    }

    // static member methods (alphabetic)

    /**
     * Returns the default dataset name build from the file name and the
     * supplied metadata.
     * 
     * @param dataFile
     *            the data file.
     * @param metadata
     *            the file's metadata.
     * @return the default dataset name build from the file name and the
     *         supplied metadata.
     */
    private static String defaultDataset(File dataFile,
                                         SpotMetadata metadata) {
        final String dataName = dataFile.getName();
        if (dataName.endsWith(HDF5_SUFFIX)) {
            return dataName.substring(0,
                                      dataName.length() - HDF5_SUFFIX.length());
        }
        return dataName;
    }

    /**
     * Reads a {@link SpotMetadata} instance from the specified file.
     * 
     * @param file
     *            the file from which to read the {@link SpotMetadata} instance.
     * @return the {@link SpotMetadata} instance from the specified file.
     * @throws ParseException
     *             when the contents of the file can not be parsed into a
     *             {@link SpotMetadata} instance.
     */
    public static SpotMetadata readMetaData(File file) throws ParseException {
        if (0L == file.length()) {
            return new SpotMetadata();
        }
        try {
            JAXBContext content = JAXBContext.newInstance(SpotMetadata.class);
            Unmarshaller unmarshaller = content.createUnmarshaller();
            final SpotMetadata result = (SpotMetadata) unmarshaller.unmarshal(file);
            return result;
        } catch (JAXBException e) {
            throw new ParseException(e);
        }
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
