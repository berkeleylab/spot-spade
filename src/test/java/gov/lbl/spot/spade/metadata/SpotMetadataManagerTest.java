package gov.lbl.spot.spade.metadata;

import static org.junit.Assert.assertEquals;
import static utils.TestUtils.createTempFile;
import static utils.TestUtils.writeBL1232Defaults;
import static utils.TestUtils.writeBL1232Semaphore;
import static utils.TestUtils.writeBL832Defaults;
import static utils.TestUtils.writeBL832Semaphore;
import static utils.TestUtils.writeDefaultBL733Metadata;
import static utils.TestUtils.writeFullBL733Semaphore;
import static utils.TestUtils.writeNoCalibrationBL733Semaphore;
import static utils.TestUtils.writeNoStitchesBL733Semaphore;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.warehouse.Location;
import gov.lbl.spot.spade.metadata.bl1232.BL1232Metadata;
import gov.lbl.spot.spade.metadata.bl733.BL733Metadata;
import gov.lbl.spot.spade.metadata.bl832.BL832Metadata;
import gov.lbl.spot.spade.warehouse.SpotWarehouse;

import java.io.File;
import java.io.PrintWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpotMetadataManagerTest  {
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNoStitched() throws Exception {
		final File bl733Defaults = createTempFile("bl733Defaults", ".xml");
		BL733Metadata defaultBL733Metadata = writeDefaultBL733Metadata(bl733Defaults);
		
		final LocalRegistration registration = new LocalRegistration() {
			public String getDefaultMetadata() {
				return bl733Defaults.getName();
			}
		};
		
		final Configuration configuration = new Configuration() {
			public File getDirectory() {
				return bl733Defaults.getParentFile();
			}
		};
		
		SpotMetadataManager manager = new SpotMetadataManager() {
			 public Configuration getConfiguration() {
			    	return configuration;
			 }
		};
		
		final File bl733FullSemaphore = createTempFile("bl733Full", ".sem");
		final BL733Metadata bL733FullSemaphore = writeNoStitchesBL733Semaphore(bl733FullSemaphore);
		
		BL733Metadata ret = (BL733Metadata) manager.createMetadata(registration, null,  bl733FullSemaphore);
		
		
		assertEquals(ret.getStage(), "raw");
		assertEquals(ret.getStage(), defaultBL733Metadata.getStage());
		assertEquals(ret.getEndStation(), "bl733");
		assertEquals(ret.getEndStation(), defaultBL733Metadata.getEndStation());
		assertEquals(ret.getStageFlow(), "/raw");
		assertEquals(ret.getStageFlow(), defaultBL733Metadata.getStageFlow());
		assertEquals(ret.getDetectors().size(), 3);
		assertEquals(ret.getDetectors().size(), bL733FullSemaphore.getDetectors().size());
		assertEquals(ret.getKeywords().size(), 2);
		assertEquals(ret.getKeywords(), bL733FullSemaphore.getKeywords());
		assertEquals(ret.getDataset(), bL733FullSemaphore.getDataset());
	}
	
	@Test
	public void testFull() throws Exception {
		final File bl733Defaults = createTempFile("bl733Defaults", ".xml");
		BL733Metadata defaultBL733Metadata = writeDefaultBL733Metadata(bl733Defaults);
		
		final LocalRegistration registration = new LocalRegistration() {
			public String getDefaultMetadata() {
				return bl733Defaults.getName();
			}
		};
		
		final Configuration configuration = new Configuration() {
			public File getDirectory() {
				return bl733Defaults.getParentFile();
			}
		};
		
		SpotMetadataManager manager = new SpotMetadataManager() {
			 public Configuration getConfiguration() {
			    	return configuration;
			 }
		};
		
		final File bl733FullSemaphore = createTempFile("bl733Full", ".sem");
		final BL733Metadata bL733FullSemaphore = writeFullBL733Semaphore(bl733FullSemaphore);
		
		BL733Metadata ret = (BL733Metadata) manager.createMetadata(registration, null,  bl733FullSemaphore);
		
		
		assertEquals(ret.getStage(), "raw");
		assertEquals(ret.getStage(), defaultBL733Metadata.getStage());
		assertEquals(ret.getEndStation(), "bl733");
		assertEquals(ret.getEndStation(), defaultBL733Metadata.getEndStation());
		assertEquals(ret.getStageFlow(), "/raw");
		assertEquals(ret.getStageFlow(), defaultBL733Metadata.getStageFlow());
		assertEquals(ret.getDetectors().size(), 3);
		assertEquals(ret.getDetectors().size(), bL733FullSemaphore.getDetectors().size());
		assertEquals(ret.getKeywords().size(), 2);
		assertEquals(ret.getKeywords(), bL733FullSemaphore.getKeywords());
		assertEquals(ret.getDataset(), bL733FullSemaphore.getDataset());
	}
	
	@Test
	public void testNocalibrationl() throws Exception {
		final File bl733Defaults = createTempFile("bl733Defaults", ".xml");
		BL733Metadata defaultBL733Metadata = writeDefaultBL733Metadata(bl733Defaults);
		
		final LocalRegistration registration = new LocalRegistration() {
			public String getDefaultMetadata() {
				return bl733Defaults.getName();
			}
		};
		
		final Configuration configuration = new Configuration() {
			public File getDirectory() {
				return bl733Defaults.getParentFile();
			}
		};
		
		SpotMetadataManager manager = new SpotMetadataManager() {
			 public Configuration getConfiguration() {
			    	return configuration;
			 }
		};
		
		final File bl733FullSemaphore = createTempFile("bl733Full", ".sem");
		final BL733Metadata bL733FullSemaphore = writeNoCalibrationBL733Semaphore(bl733FullSemaphore);
		
		final BL733Metadata ret = (BL733Metadata) manager.createMetadata(registration, null,  bl733FullSemaphore);
		
		
		assertEquals(ret.getStage(), "raw");
		assertEquals(ret.getStage(), defaultBL733Metadata.getStage());
		assertEquals(ret.getEndStation(), "bl733");
		assertEquals(ret.getEndStation(), defaultBL733Metadata.getEndStation());
		assertEquals(ret.getStageFlow(), "/raw");
		assertEquals(ret.getStageFlow(), defaultBL733Metadata.getStageFlow());
		assertEquals(ret.getDetectors().size(), 3);
		assertEquals(ret.getDetectors().size(), bL733FullSemaphore.getDetectors().size());
		assertEquals(ret.getKeywords().size(), 2);
		assertEquals(ret.getKeywords(), bL733FullSemaphore.getKeywords());
		assertEquals(ret.getDataset(), bL733FullSemaphore.getDataset());
	}

	@Test
	public void testBL832() throws Exception {
		final File bl832Defaults = createTempFile("bl832Defaults", ".xml");
		final BL832Metadata defaultBL832Metadata = writeBL832Defaults(bl832Defaults);
		final LocalRegistration registration = new LocalRegistration() {
			public String getDefaultMetadata() {
				return bl832Defaults.getName();
			}
		};
		
		final Configuration configuration = new Configuration() {
			public File getDirectory() {
				return bl832Defaults.getParentFile();
			}
		};
		
		final SpotMetadataManager manager = new SpotMetadataManager() {
			 public Configuration getConfiguration() {
			    	return configuration;
			 }
		};
		
		final File bl832FullSemaphore = createTempFile("bl832", ".sem");
		final BL832Metadata bL832FullSemaphore = writeBL832Semaphore(bl832FullSemaphore);
		final File dataFile = new File("ds1.h5");
		final BL832Metadata ret = (BL832Metadata) manager.createMetadata(registration, dataFile,  bl832FullSemaphore);
		
		
		assertEquals(ret.getStage(), "raw");
		assertEquals(ret.getStage(), defaultBL832Metadata.getStage());
		assertEquals(ret.getEndStation(), "bl832");
		assertEquals(ret.getEndStation(), defaultBL832Metadata.getEndStation());
		assertEquals(ret.getStageFlow(), "/raw");
		assertEquals(ret.getStageFlow(), defaultBL832Metadata.getStageFlow());
		assertEquals(ret.getDataPath(), bL832FullSemaphore.getDataPath());
		assertEquals(ret.getDataset() + ".h5", dataFile.getName());
	}
	
	@Test
	public void testBL1232() throws Exception {
		final File bl1232Defaults = createTempFile("bl1232Defaults", ".xml");
		final BL1232Metadata defaultBL1232Metadata = writeBL1232Defaults(bl1232Defaults);
		
		final LocalRegistration registration = new LocalRegistration() {
			public String getDefaultMetadata() {
				return bl1232Defaults.getName();
			}
		};
		
		final Configuration configuration = new Configuration() {
			public File getDirectory() {
				return bl1232Defaults.getParentFile();
			}
		};
		
		final SpotMetadataManager manager = new SpotMetadataManager() {
			 public Configuration getConfiguration() {
			    	return configuration;
			 }
		};
		
		final File bl1232FullSemaphore = createTempFile("bl1232", ".sem");
		final BL1232Metadata bL1232FullSemaphore = writeBL1232Semaphore(bl1232FullSemaphore);
		final File dataFile = new File("ds1.h5");
		final BL1232Metadata ret = (BL1232Metadata) manager.createMetadata(registration, dataFile,  bl1232FullSemaphore);
		
		
		assertEquals(ret.getStage(), "raw");
		assertEquals(ret.getFacility(), "als");
		assertEquals(ret.getStage(), defaultBL1232Metadata.getStage());
		assertEquals(ret.getEndStation(), "bl1232");
		assertEquals(ret.getEndStation(), defaultBL1232Metadata.getEndStation());
		assertEquals(ret.getStageFlow(), "/raw");
		assertEquals(ret.getStageFlow(), defaultBL1232Metadata.getStageFlow());
		assertEquals(ret.getDataPath(), bL1232FullSemaphore.getDataPath());
		assertEquals(ret.getDataset(), bL1232FullSemaphore.getDataset());
		assertEquals(ret.getFirst(), bL1232FullSemaphore.getFirst());
		assertEquals(ret.getLast(), bL1232FullSemaphore.getLast());
		assertEquals(ret.getExt(), bL1232FullSemaphore.getExt());
	}
	
	public static void main(String[] args) throws Exception {
		new SpotMetadataManagerTest().testBL1232();
	}
}
