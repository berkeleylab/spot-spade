package gov.lbl.spot.spade.metadata.bl733;

import gov.lbl.spot.spade.metadata.SpotMetadata;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement(name = "bl733-metadata")
@XmlType(propOrder = { "dataPath",
                       "detectors",
                      "keywords"}) 

public class BL733Metadata extends SpotMetadata {
	
    private String dataPath;

    private List<String> keywords;
    
    private List<Detector> detectors;
    
    /**
     * Returns the path to the original data.
     * 
     * @return the path to the original data.
     */
    @XmlElement(name = "data_path")
    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }
	@XmlElement(name = "detector")
	public List<Detector> getDetectors() {
		return detectors;
	}

	public void setDetectors(List<Detector> detectors) {
		this.detectors = detectors;
	}

	@XmlElementWrapper
	@XmlElement(name = "keyword")
	public List<String> getKeywords() {
		if (keywords != null) {
			List<String> ret = new ArrayList<String>();
			
			for (String kw: keywords) {
				if (kw != null && kw.trim().length() != 0) {
					ret.add(kw.trim());
				}
			}
			
			return ret;
		}
		
		return null;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}
}
