package gov.lbl.spot.spade.policy;

import gov.lbl.nest.spade.config.Policy;
import gov.lbl.nest.spade.metadata.Metadata1Impl;
import gov.lbl.nest.spade.policy.FailedPolicyException;
import gov.lbl.nest.spade.policy.PlacingPolicy;
import gov.lbl.nest.spade.registry.ParseException;
import gov.lbl.nest.spade.services.MetadataManager;
import gov.lbl.spot.spade.metadata.SpotMetadata;
import gov.lbl.spot.spade.metadata.SpotMetadataManager;

import java.io.File;

/**
 * This class implements the {@link PlacingPolicy} instance so that files are
 * placed with respect to their facility/end_stattion/owner/dataset/stage
 * 
 * @author patton
 */
public class SpotPlacing implements
                        PlacingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The class to use to read the XML content of the metadata file.
     */
    private final Class<? extends SpotMetadata> metadataClass;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param policy
     *            the {@link Policy} instance describing the instance to create.
     */
    @SuppressWarnings("unchecked")
    public SpotPlacing(final Policy policy) {
        final ClassLoader loader = getClass().getClassLoader();
        final String clazz = policy.getParameter("metadataClass");
        if (null == clazz) {
            metadataClass = SpotMetadata.class;
        } else {
            try {
                metadataClass = (Class<? extends SpotMetadata>) loader.loadClass(clazz);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                throw new IllegalArgumentException(e);
            }
        }
    }

    // instance member method (alphabetic)

    // constructors

    // instance member method (alphabetic)

    @Override
    public File getArchivePlacement(File metadataFile) throws FailedPolicyException {
        return getMetadataPlacement(metadataFile);
    }

    @Override
    public File getCompressedPlacement(String fileName,
                                       File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getDataPlacement(String fileName,
                                 File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getMetadataPlacement(File metadataFile) {
        return new File(path(metadataFile),
                        metadataFile.getName());
    }

    @Override
    public File getWrappedPlacement(String fileName,
                                    File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    /**
     * Returns the directory indicated by the {@link Metadata1Impl} instance.
     * 
     * @param metadataFile
     *            the cached metadata file
     * @return the directory indicated by the {@link Metadata1Impl} instance.
     */
    private File path(final File metadataFile) {
        SpotMetadata metadata;
        try {
            metadata = SpotMetadataManager.readMetaData(metadataFile);
        } catch (ParseException e) {
            throw new IllegalArgumentException("Metadata can not be parsed",
                                               e);
        }
        final String facility = metadata.getFacility();
        if (null == facility) {
            throw new IllegalArgumentException("No facility specified in metadata");
        }
        final String endStation = metadata.getEndStation();
        if (null == endStation) {
            throw new IllegalArgumentException("No end_station specified in metadata");
        }
        final String owner = metadata.getOwner();
        if (null == owner) {
            throw new IllegalArgumentException("No owner specified in metadata");
        }
        final String dataset = metadata.getDataset();
        if (null == dataset) {
            throw new IllegalArgumentException("No dataset specified in metadata");
        }
        final String stage = metadata.getStage();
        if (null == stage) {
            throw new IllegalArgumentException("No stage specified in metadata");
        }
        return new File(new File(new File(new File(facility,
                                                   endStation),
                                          owner),
                                 dataset),
                        stage);
    }

    @Override
    public void setMetadataManager(MetadataManager manager) {
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
